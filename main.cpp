/*

TO DO :
  creation d'un graphe à la main
  -> meilleure intuitivité
   -opérations à considérer
    add vertex, add edge
    remove vertex, remove edge
    cliquifier en sélectionnant en carré
  - auto placement elastique    
    
TO DO :
  -sauvegarde
  - cartes planaires
  - dual
  - afficher le nom
  - couleur

  

TO DO:
  SW : générer tous, les unique shelling order
  exporter vers svg pdf ipe
  bouger la base polyedral
  
  handhaking problem -> coloration
  




*/



#include <SFML/Graphics.hpp>


#include<iostream>
#include <fstream>
#include<vector>
#include<list>
#include<map>
#include<time.h>
#include<string>
#include<queue>
#include<math.h>

typedef long long int long_t;

#include "src/graphe.cpp"

#define PI 3.141592654

#define COL_PH  sf::Color(200,200,200)

#define point_radius 14
#define COL_VERTEX  sf::Color(255, 204, 255)
sf::CircleShape shapeVertex(point_radius);


// Stretch compute ?

bool tool_stretch = false;

bool tool_delaunay_constructions = true; 

bool tool_SW = false;


using namespace std;



// --------------------------------
// Geometric tools for sf::Vector2f
// TO DO

float dotproduct( sf::Vector2f p , sf::Vector2f q ){
  return p.x *q.x + p.y * q.y ;
}

sf::Vector2f normalize( sf::Vector2f p ){
  float d = sqrt(dotproduct(p,p));
  return sf::Vector2f(p.x/d , p.y/d);
}

sf::Vector2f rothoraire( sf::Vector2f p )
  {
    return sf::Vector2f(p.y , -p.x);
  }






/*
typedef struct coord coord;
struct coord
{
float x;
float y;
};
*/

class coord{
  public:
    float x;
    float y;
    
    coord();
    coord( float u, float v );

    sf::Vector2f toVect();
    void normer();
};

coord::coord() { x = 0; y = 0; }

coord::coord( float u , float v) { x = u; y = v; }

sf::Vector2f coord::toVect () { return sf::Vector2f( x , y ); }

void coord::normer( )
  {
    float d = sqrt(x*x + y*y);
    x = x/d;
    y = y/d;  
  }




vector<coord> points;

Graph G;



sf::RenderWindow window(sf::VideoMode(1000, 1000), "Stretch"); 
sf::Font font;


int current_point = -1;
int current_add_edge_point = -1;

    
int dist2( int i, int j )
  {
    return (points[i].x - points[j].x)*(points[i].x - points[j].x) + (points[i].y - points[j].y)*(points[i].y - points[j].y) ;
  }

float dotproduct( coord p , coord q )
  {
    return p.x *q.x + p.y * q.y ;
  }
  
coord rothoraire( coord p )
  {
    return coord(p.y , -p.x);
  }
  
float dist2( coord p , coord q )
  {
    return sqrt( (p.x - q.x)*(p.x - q.x) + (p.y - q.y)*(p.y - q.y) );
  }
  
  
// MODE


#define GRAPH_MODE_GEOM 0
#define GRAPH_MODE_D2 1
#define GRAPH_MODE_TDD 2
#define GRAPH_MODE_W 3
#define GRAPH_MODE_A_LA_MAIN 4
#define GRAPH_MODE_MOLEKULARIS 5

#define GRAPH_MODE_TOTAL 6

bool remove_mode = false;
int graph_mode; 
string graph_mode_string;

void init_graph_mode_string()
  {
  
   switch(graph_mode)
    {
      case GRAPH_MODE_GEOM:
        graph_mode_string = "Geometrique";
        break;
      case GRAPH_MODE_D2:
        graph_mode_string = "L2 Delaunay";
        break;
      case GRAPH_MODE_TDD:
        graph_mode_string = "TD Delaunay";
        break;  
      case GRAPH_MODE_W:
        graph_mode_string = "W Delaunay";
        break;  
      case GRAPH_MODE_A_LA_MAIN:
        graph_mode_string = "A la main"; 
        break;
      case GRAPH_MODE_MOLEKULARIS:
        graph_mode_string = "Molekularis";     
        break;
    }
    
  }
  


   
   
// ==========================================================
// ==========================================================
// MOLEKULARIS

   
vector<int> mol_phi;
vector< vector<int> > mol_liaisons;

vector< vector<int> > m; // m est la matrice d'adjacence du graphe
vector<int> a_donner; // a_donner(v) = ce quil reste à donner = varmol_phi(v) - sum_(arretes v) poids(v)
vector<int> a_recuperer; // a_recuperer(v) = sum_(voisins v) a_recuperer(u)
// a_donner <= a_recuperer


 
 
 
// ==========================================================
// ==========================================================

// graph

int max_stretch_i = -1;
int max_stretch_j = -1;

void initGraph()
  {
    G = Graph();
    max_stretch_i = -1;
    max_stretch_j = -1;
    for ( int i = 0 ; i < points.size() ; i ++)
      G.add_vertex(i);
  }


// stretch

vector< vector<float> > A; // matrice adjacence pondérée

void remplirA ()
  {
    
    int n = points.size();
    A.resize(n);
    for ( int i = 0 ; i < n ; i ++ )
      A[i].resize(n);
    
    for ( int i = 0 ; i < n ; i ++)
      for ( int j = 0 ; j < n ; j ++ )
        {
          A[i][j] = 1000000.;          
          if ( G.is_edge(i,j) )
            {
            A[i][j] = dist2( points[i] , points[j] );                   
            }
        }          
  }
  
  void imprimerA ()
  {
    int n = points.size();
      for ( long i = 0 ; i < n ; i ++)
    {
      for (long j = 0 ; j < n ; j ++ )
        cout << A[i][j] << " ";
     cout << endl;
     }   
     
  
  }
  

void FloydWarshall ()
  {
    int n = points.size();
    for ( long k = 0 ; k < n ; k ++ )
      for ( long i = 0 ; i < n ; i ++ )
        for ( long j = 0 ; j < n ; j ++ )
          A[i][j] = min( A[i][j] , A[i][k] + A[k][j]);
  }
  



float stretch ()
  {
    float s = 0.;
    int n = points.size();
    
    for ( long i = 0 ; i < n ; i ++ )
      for ( long j = i+1 ; j < n ; j ++ )
        {
          float di = dist2( points[i] , points[j] );
          if ( A[i][j] > di * s )
            {
              max_stretch_i = i;
              max_stretch_j = j;
              s = A[i][j]/di;
            }
        }
        
    return s;
  }



// L2 Delaunay


coord cercleCirconscrit( coord p, coord q , coord r )
  {
    float cy;
    float cx;
    
    float px = p.x;
    float py = p.y;
    float qx = q.x;
    float qy = q.y;
    float rx = r.x;
    float ry = r.y;
    float PP = px*px + py*py;
    float QQ = qx*qx + qy*qy;
    float RR = rx*rx + ry*ry;
    
    cy = QQ - PP - (RR - PP)*(qx-px)/(rx-px);
    cy /= 2*( qy-py - (ry-py)*(qx-px)/(rx-px) );
    
    cx = (RR - PP - 2 *cy *(ry - py))/ (2*(rx-px));
    
    coord c;
    c.x = cx;
    c.y = cy;
  
    return c;
  }

void computeD2()
  {
    initGraph();
    int n = points.size();
    
    for( int i = 0 ; i < n ; i ++ )
      for ( int j = i+1 ; j < n ; j ++ )
        for ( int k = j+1 ; k < n ; k ++ )
          {
            
            coord c = cercleCirconscrit( points[i], points[j], points[k] );
            float dis = dist2( points[i] , c);
            
            bool b = true;
            for( int u = 0 ; u < n ; u ++ )
            if ( u != i && u != j && u != k )
              {                
                if ( dist2( points[u] , c) < dis )
                  { b = false; break; }              
              }
            if ( b)
              {
              G.add_edge( i,j);
              G.add_edge(i,k);
              G.add_edge(j,k);
              }          
          }
  
  }



// =============================================================================
// =============================================================================
// =============================================================================

// TDD

coord base[3];

bool dominate( int x , list<int> L , vector<int> rep[3])
  {
    for ( int d = 0 ; d < 3 ; ++ d )
      {
        list<int> U;
        U = L;
        for( int i= 0 ; i < rep[d].size() ; i ++ )
          {
            U.remove( rep[d][i] );
            if ( U.empty() )
              return true;

            if ( rep[d][i] == x )
              {
                break;
              }
          }
      
      }
    return false;
  }

bool isFace( list<int> L , vector<int> rep[3] )
{
  for ( int x = 0 ;  x < points.size() ; x ++ )
      if ( dominate(x, L, rep ) == false )
        return false;
        
  return true;
}

int edgeDir( int i, int j , vector<int> rep[3] )
  {
    bool b0,b1,b2;
    int i_index[3];
    int j_index[3];
    
    for ( int d = 0 ; d < 3 ; ++ d )
      for ( int k = 0 ; k < rep[d].size() ; ++ k )
        {
          if( rep[d][k] == i )
            i_index[d] = k;
          else if ( rep[d][k] == j )
            j_index[d] = k;        
        }  
    b0 = i_index[0] < j_index[0];
    b1 = i_index[1] < j_index[1];
    b2 = i_index[2] < j_index[2];
    
    if ( b0 && !b1 && !b2 )
      return 1;
    else if (!b0 && b1 && !b2)
      return 2;
    else if (!b0 && !b1 && b2)
      return 3;
    else if ( !b0 && b1 && b2 )
      return -1;
    else if (b0 && !b1 && b2)
      return -2;
    else if (b0 && b1 && !b2)
      return -3;  
    else
      return 0;
  }
  
  
  
void printRep( vector<int> rep[3] )
  {
    for ( int d = 0 ; d < 3 ; ++ d )
      {
        for ( int i = 0 ; i < rep[d].size() ; ++ i )
          cout << rep[d][i];          
        cout << endl;
      }
  
  }

vector<int> rep[3];

void computeTDD()
  {
    initGraph();
    for ( int d = 0 ; d < 3 ; ++ d)
      rep[d] = vector<int>(0);    
    

    
    for (  int d = 0 ; d < 3 ; ++ d  )
      {
        for ( int i = 0 ; i  < points.size() ; i ++  )
        {
          bool b = false;
          for ( vector<int>::iterator it = rep[d].begin(); it != rep[d].end() ; ++ it )
            {
            // avant c'était >=
              if ( dotproduct( points[i], base[d]) < dotproduct( points[*it], base[d] ) )
                {
                  rep[d].insert( it , i );                  
                  b = true;
                  break;
                } 
            }
            
          if( !b )
            rep[d].push_back(i);
         }
      }
      
  
for ( int i = 0 ; i < points.size() ; i ++ )
      for ( int j = i+1 ; j < points.size() ; j ++ )
        {
          /* bool b = true;
          for ( int x = 0 ;  x < points.size() ; x ++ )
            {
              if ( dominate(x, {i,j} , rep ) == false )
                {
                b = false;
                break;
                }
            }
            */
            
            if ( isFace( {i,j}, rep) )
              {    
                switch( edgeDir (i,j,rep) )   
                  {
                  case 1:
                    G.add_arc(j,i, sf::Color::Red);
                    break;
                  case -1:
                    G.add_arc(i,j, sf::Color::Red);                  
                    break;
                  case 2:
                    G.add_arc(j,i, sf::Color::Green);
                    break;
                  case -2:
                    G.add_arc(i,j, sf::Color::Green);  
                    break;
                  case 3:
                    G.add_arc(j,i, sf::Color::Blue);
                    break;
                  case -3:
                    G.add_arc(i,j,sf::Color::Blue);                        
                    break;
                  }     
              }
        }
        
    
  }
  
  
// à généraliser en dim + grande ?
vector< vector<float> >  inverseMatrix( vector< vector<float> > m )
{
  float det = m[0][0] * m[1][1] - m[0][1] * m[1][0];
  vector< vector<float> > mm;
  mm.resize(2);
  mm[0].resize(2);
  mm[1].resize(2);

  mm[0][0] = m[1][1]/det;
  mm[1][1] = m[0][0]/det;
  mm[0][1] = -m[0][1]/det;
  mm[1][0] = -m[1][0]/det;

  return mm;
}
  
  
// calcule le polyhedre associé à la face F
list<coord>  computePolyhedra( list<int> F )
{
  int wl = 3; // taille de la base de vecteurs
  list<coord> l;   
  float tab_max[wl];

  for(list<int>::iterator it = F.begin() ; it != F.end() ; ++ it)
  {
    for ( int k = 0 ; k < wl ; k ++ ) 
      if ( it == F.begin() )
        tab_max[k] = dotproduct( points[*it] , base[k] );
      else
        tab_max[k] = max( tab_max[k] , dotproduct( points[*it] , base[k] ) );    
      }


  // il y a 2 boucles car on est en dim 2
  // il est possible qu'il n'y ait pas d'intersection -> seulement quand c'est une base de R^d ?
  for ( int i = 0 ; i < wl ; i ++ )
    for ( int j = i+1 ; j < wl ; j ++ )
      {
        //on cherche le point de coord x = (u,v) tel que <x, base[i]> = tab_max[i] et <x,base[j]> = tab_max[j]

        vector< vector<float> > m;
        m.resize(2);
        m[0].resize(2);
        m[1].resize(2);
        m[0][0] = base[i].x;
        m[0][1] = base[i].y;
        m[1][0] = base[j].x;
        m[1][1] = base[j].y;
        vector< vector<float> > mm = inverseMatrix( m);
        
        coord a = {mm[0][0]*tab_max[i] + mm[0][1]*tab_max[j], mm[1][0]*tab_max[i] + mm[1][1]*tab_max[j]};

      l.push_back(a);
      }

  return l;
}


// V2
list<coord>  computePolyhedra2( list<int> F )
{
  int wl = 3; // taille de la base de vecteurs
  list<coord> l;   
  float tab_max[wl];

  for(list<int>::iterator it = F.begin() ; it != F.end() ; ++ it)
  {
    for ( int k = 0 ; k < wl ; k ++ ) 
      if ( it == F.begin() )
        tab_max[k] = dotproduct( points[*it] , base[k] );
      else
        tab_max[k] = max( tab_max[k] , dotproduct( points[*it] , base[k] ) );    
  }


  // il y a 2 boucles car on est en dim 2
  // il est possible qu'il n'y ait pas d'intersection -> seulement quand c'est une base de R^d ?
  for ( int i = 0 ; i < wl ; i ++ )
  {
    list<coord> inter_point;
    
    for ( int j = 0 ; j < wl ; j ++ )
    if( j != i )
      {
        vector< vector<float> > m;
        m.resize(2);
        m[0].resize(2);
        m[1].resize(2);
        m[0][0] = base[i].x;
        m[0][1] = base[i].y;
        m[1][0] = base[j].x;
        m[1][1] = base[j].y;
        
        if ( m[0][0]*m[1][1] - m[0][1]*m[1][0] != 0 )
          {
        vector< vector<float> > mm = inverseMatrix( m);
        
        coord a = {mm[0][0]*tab_max[i] + mm[0][1]*tab_max[j], mm[1][0]*tab_max[i] + mm[1][1]*tab_max[j]};

      bool ok = true;
      for ( int k = 0 ; k < wl ; k ++ )
        if ( k != i && k != j && dotproduct(a,base[k]) > tab_max[k])
          ok = false;
          
      if ( ok )
        inter_point.push_back(a);
        }
      }
      
    if( inter_point.size() == 2 )
      {
        l.push_back( inter_point.back());
        inter_point.pop_back();
        l.push_back( inter_point.back());
      }      
    else if( inter_point.size() == 1 )
      {
        coord vitg; // tourné pi/2 vers la gauche
        vitg.x = -base[i].y;
        vitg.y = base[i].x;
        
        coord ip = inter_point.back();
        coord nip;
        
        
        int j = i+1;
        if ( j == wl )
          j = 0;
          
        if ( dotproduct(vitg,base[j]) < 0 )
          {
            l.push_back( ip);
            nip.x = ip.x + vitg.x*1000;
            nip.y = ip.y + vitg.y*1000;
            l.push_back( nip);            
          }
        else
          {
            l.push_back( ip);
            nip.x = ip.x - vitg.x*1000;
            nip.y = ip.y - vitg.y*1000;
            l.push_back( nip);            
          }

        
      
      }
  
  }

  return l;
}
  

// =================================
// =================================
// Schnyder Woods


class SW{
    
  public:
    map< Edge , int > _f;
    SW(); 
    int val(Edge e);
    void apply();
};

SW::SW ()
  {
    list<Edge> edges = G.edges();
    for ( list<Edge>::iterator it = edges.begin() ; it != edges.end() ; ++ it )
      {
      Edge e = *it;
      _f[e] = 0;      
      }  
  }
  
int SW::val(Edge e)
  {
    return _f[e];
  }
  
void SW::apply()
  {
  list<Edge> edges = G.edges();
    for ( list<Edge>::iterator it = edges.begin() ; it != edges.end() ; ++ it )
      {
      Edge e = *it;
      if ( _f[e] > 0 )
        e.change_dir(1);
      else
        e.change_dir(-1);
      }    
  }
  

vector<SW> SWdeG;
int SWnumAffiche = 0; // numéro du SW affiché


// trouver tous les SW de G
/*
void findAllSW( void )
  {
    int nt = 0; // numero de celui en cours de traitement
    
    // rajouter le basique
    TO DO
    
    while ( nt != SWdeG.size() )
      {
        // chercher un 3cycle
        // créer un nouveau SW avec inversion du cycle
        // regarder s'il existe deja
        // sinon on le rajoute au tas SWdeG
        
        // - chercher un 3cycle
        // regarder parmi toutes les faces
        // regarder si c'est un cycle
        
        for ( int i = 0 ; i < points.size() ; ++ i )
        for ( int j = i+1 ; j < points.size(); ++ j )
        for ( int k = j+1 ; k < points.size(); ++ k )
        if ( isFace( {i,j,k}, rep ) )
          {
            Edge eij;
            Edge ejk;
            Edge eki;
            // calcul des edges TO DO
            TO DO
            int bij = (eij.start() == i) ? 1 : -1;
            int bjk = (ejk.start() == j) ? 1 : -1;
            int bki = (eki.start() == k) ? 1 : -1;
            
            map<Edge,int> lol = SWdeG[nt]._f;

            // si cycle ok
            
            if ( (bij*lol[eij] == 1 && bjk*lol[ejk] == 2 && bki*lol[eki] == 3) 
            || (bij*lol[eij] == 2 && bjk*lol[ejk] == 3 && bki*lol[eki] == 1) 
            || (bij*lol[eij] == 3 && bjk*lol[ejk] == 1 && bki*lol[eki] == 2)
            || (bij*lol[eij] == -1 && bjk*lol[ejk] == -2 && bki*lol[eki] == 3)
            || (bij*lol[eij] == -2 && bjk*lol[ejk] == -3 && bki*lol[eki] == -1)
            || (bij*lol[eij] == -3 && bjk*lol[ejk] == -1 && bki*lol[eki] == -2) )
              {
               // création nouveau cycle
                SW newsw = SWdeG[nt];
                
                if ( bij*lol[eij] == 1 )
                  {
                    newsw._f[eij] = -2*bij;
                    newsw._f[ejk] = -3*bjk;
                    newsw._f[eki] = -1*bki;
                  }
                else if ( bij*lol[eij] == 2 )
                  {
                    newsw._f[eij] = -3*bij;
                    newsw._f[ejk] = -1*bjk;
                    newsw._f[eki] = -2*bki;
                  }
                else if ( bij*lol[eij] == 3 )
                  {
                    newsw._f[eij] = -1*bij;
                    newsw._f[ejk] = -2*bjk;
                    newsw._f[eki] = -3*bki;
                  }
                else if ( bij*lol[eij] == -1 )
                  {
                    newsw._f[eij] = 2*bij;
                    newsw._f[ejk] = 1*bjk;
                    newsw._f[eki] = 3*bki;
                  }               
                else if ( bij*lol[eij] == -2 )
                  {
                    newsw._f[eij] = 3*bij;
                    newsw._f[ejk] = 2*bjk;
                    newsw._f[eki] = 1*bki;
                  }               
                else if ( bij*lol[eij] == -3 )
                  {
                    newsw._f[eij] = 1*bij;
                    newsw._f[ejk] = 3*bjk;
                    newsw._f[eki] = 2*bki;
                  }            
                 
               bool b = true;                         
               for ( int a = 0 ; a < SWdeG.size() ; ++ a )
                {
                  bool bb = true;
                  list<Edge> edges = G.edges();
                    for ( list<Edge>::iterator it = edges.begin() ; it != edges.end() ; ++ it )
                    {
                      Edge e = *it;
                      if ( newsw._f[e] != SWdeG[a]._f[e] )
                        {
                          bb = false;
                          break;
                        }
                    }
                    if ( bb )
                      {b = false; break;}
                }
                
                
                if ( b )
                SWdeG.push_back( newsw );
                 
                
              
              }
            
            
          
          }
      
        nt++;
      }
  
  
  }
  */






// =================================
// =================================
// Géométrique
void computeGeom( )
  {
    initGraph();
  
    for ( int i = 0 ; i < points.size() ; i ++ )
      for ( int j = i+1 ; j < points.size() ; j ++ )
        if ( dist2(i,j) < 10000 )
          {
          G.add_edge( i , j);
          }
  
  }
    
    
    
 
 
// ========================
// placement en cercle des points

int N = 10;
float pp = 2;

void putLp(  )
  {
    points.resize(N+1);
    float h = 2./N;    
    for ( int k = 0 ; k <= N ; k ++ )
      {
        points[k].x = -1 + k*h;
        points[k].y = pow(-1,k) * pow(1 - pow( abs(points[k].x) , pp ), 1./pp);      
        points[k].x = 500 + 300*points[k].x;
        points[k].y = 500 + 200*points[k].y;
      }      
  }
  
float LLh=50;
float LLv = 100;
float HH = 5;
float LLc = 100;
float U = 300;
float V = 300; 

void putOcto(  )
  {
    
    points.resize(0);
    for ( int k = 0 ; k*HH <= 2*LLc; k ++ )
      {
        coord newp;
        points.push_back( newp );
        points[k].x = -LLc + (k*HH);
        points[k].y = 0.99* sqrt(LLc*LLc - points[k].x*points[k].x);      
        

        points[k].x += LLc + U;
        if ( k*HH > LLc )
          points[k].x += LLh;
        
        if ( k%2 == 0 )
          {
          
          points[k].y += LLc + V + LLv;
          }
        else
          {
          
          points[k].y *= -1;
          points[k].y += LLc + V;
          }       
        
      }      
  }
  
  



// Calcul du graphe + du stretch
void computeGraph()
  {
    switch(graph_mode)
    {
      case GRAPH_MODE_GEOM:
        computeGeom();
        break;
      case GRAPH_MODE_D2:
        computeD2();
        break;
      case GRAPH_MODE_TDD:
        computeTDD();
        break;  
      case GRAPH_MODE_W:
        computeTDD();
      break;
    }
      
    
    remplirA();
    FloydWarshall();
    //cout << "stretch : " << stretch() << endl;
  }
    


//

// =======================================
// fonction pour l'interface

// isPointNear
// int x, int y : les coordonnées de la ou on clique
// renvoie -1 si pas clic sur un sommet
// renvoie l'indice du sommet sur lequel on a cliqué sinon
int isPointNear( int x , int y ){  
  for( int i = 0 ; i < points.size() ; i ++ ){
    if ( abs(x - points[i].x) < point_radius && abs(y - points[i].y) < point_radius )
      return i;
  }
    return -1;
}


sf::Vector2i clic_on_edge( int x, int y ){
  int n = points.size();
  sf::Vector2f r = sf::Vector2f(x,y);

  for( int i = 0 ; i < n ; i ++ )
    for ( int j = i+1 ; j < n ; j ++ )
      if ( G.is_edge(i,j) ){
      
      sf::Vector2f p = points[i].toVect();
      sf::Vector2f q = points[j].toVect();
      sf::Vector2f u = (q-p);
      u = normalize(u);
      sf::Vector2f v = rothoraire(u);
      float h = 10;
      float h2 = point_radius;
      
      if ( dotproduct( v, r- (p- (v*h)) ) > 0 && dotproduct( v, r- (p+ (v*h)) ) < 0 && dotproduct( u, r - (p+u*h2)) > 0 && dotproduct( u, r - (q-u*h2)) < 0 )
        return sf::Vector2i(i,j);
    }
      
  return sf::Vector2i(-1,-1);
}
  
  

 
 
 
 // text_vertex est la figure qu'on affiche pour le numéro du vertex
 // utilisé dans la fonction d'affichage des sommets en la placant au bon endroit et en mettant le bon texte dedans
 sf::Text text_vertex;
 // initialisation de la figure au début du main
 void aff_init_text_vertex()
  {
    text_vertex.setFont(font);
    text_vertex.setCharacterSize(20); // exprimée en pixels, pas en points !
    text_vertex.setColor(sf::Color::Black);
  }
 
 
// aff_vertices :
// affichage des sommets
// si molekularis : affiche le petit H O N C au dessus
void aff_vertices(){ 
  for( int i = 0 ; i < points.size() ; ++ i ){
    shapeVertex.setPosition(points[i].x - point_radius ,points[i].y -point_radius);
    //shapeVertex.setOutlineThickness(3);
    //shapeVertex.setOutlineColor(sf::Color::Black);
    shapeVertex.setFillColor(COL_VERTEX);
    if ( graph_mode == GRAPH_MODE_MOLEKULARIS && a_donner[i] == 0)
      shapeVertex.setFillColor(sf::Color::Green);
    if ( graph_mode == GRAPH_MODE_MOLEKULARIS && a_donner[i] == a_recuperer[i] && a_donner[i] > 0)  
      shapeVertex.setFillColor(sf::Color::Red);
    shapeVertex.setRadius(point_radius);  
    window.draw(shapeVertex);

    // affichage de l'indice dans points du sommet
    if ( graph_mode != GRAPH_MODE_MOLEKULARIS ){
      text_vertex.setString(to_string(i));
      text_vertex.setPosition(points[i].x - 6 ,points[i].y -12);
      window.draw(text_vertex);
    }
    
    if ( graph_mode == GRAPH_MODE_MOLEKULARIS ){
      switch(mol_phi[i]){
        case 1:
          text_vertex.setString("H");
          break;
        case 2:
          text_vertex.setString("O");
          break;
        case 3:
          text_vertex.setString("N");
          break;
        case 4:
          text_vertex.setString("C");
          break;
      }      
      text_vertex.setPosition(points[i].x - 6  ,points[i].y -12 );
      window.draw(text_vertex);    
    }
    
    
    /*
    if ( tool_stretch && (max_stretch_i == i || max_stretch_j == i) )
      shapeVertex.setFillColor(sf::Color::Red);
    else
      shapeVertex.setFillColor(COL_VERTEX);

    shapeVertex.setRadius(point_radius);  
    window.draw(shapeVertex);        
    */
    
  }
}
 
void aff_cercles()
{
    for( int i = 0 ; i < points.size() ; ++ i )
      for ( int j = i+1 ; j < points.size(); ++ j)
        for ( int k = j+1 ; k < points.size() ; ++ k )
          {
            coord c = cercleCirconscrit( points[i], points[j] , points[k] );
            shapeVertex.setPosition(c.x - point_radius ,c.y -point_radius);
            shapeVertex.setFillColor(sf::Color::Blue);
            window.draw(shapeVertex);          
          }
}

void aff_construction_Delaunay()
  {
  for( int i = 0 ; i < points.size() ; ++ i )
      for ( int j = i+1 ; j < points.size(); ++ j)
        for ( int k = j+1 ; k < points.size() ; ++ k )
          {
            if ( G.is_edge(i,j) && G.is_edge(j,k) && G.is_edge(k,i) )
            {
              coord c = cercleCirconscrit( points[i], points[j] , points[k] );
              sf::CircleShape circle;
              float radius = dist2(c,points[i]);
              circle.setFillColor(sf::Color::Transparent);
              circle.setRadius(radius);
              circle.setOutlineColor(COL_PH);
              circle.setOutlineThickness(1);
              circle.setPosition(c.x - radius ,c.y - radius);
              window.draw(circle);
            }
          }  
  }


// cas où on utilise une base
void aff_polyedra1()
  {
       list<Edge> edges = G.edges();
       

    for ( list<Edge>::iterator it = edges.begin() ; it != edges.end() ; ++ it )
      {
        Edge e = *it;
        int i = e.start().num();
        int j = e.end().num();
      
        list<coord> l = computePolyhedra( {i,j} );
        sf::Vertex polyhedra[2*l.size()];
        int k = 0;
        
        for ( list<coord>::iterator it2 = l.begin(); it2 != l.end() ; ++ it2 )
          {
            polyhedra[k] = sf::Vertex( (*it2).toVect(), COL_PH);
            if ( k == 0 )
              polyhedra[2*l.size()-1] = sf::Vertex( (*it2).toVect(), COL_PH);
            else
              polyhedra[k-1] = sf::Vertex( (*it2).toVect(), COL_PH);
              
            k+=2;
           
           
          // shapeVertex.setPosition((*it2).x - point_radius-2 ,(*it2).y -point_radius);
           // shapeVertex.setFillColor(sf::Color::Black);
           // window.draw(shapeVertex);
            

          }  
          
          window.draw( polyhedra, 6, sf::Lines);  
  
    }
  }
  
  
  //=========================================
  //=========================================
void aff_polyedra2()
  {
    list<Edge> edges = G.edges();      
    for ( list<Edge>::iterator it = edges.begin() ; it != edges.end() ; ++ it )
      {
        Edge e = *it;
        int i = e.start().num();
        int j = e.end().num();
        
        list<coord> l = computePolyhedra2( {i,j} );
        sf::Vertex polyhedra[l.size()];
        int k = 0;
  
        for ( list<coord>::iterator it2 = l.begin(); it2 != l.end() ; ++ it2 )
          {
            polyhedra[k] = sf::Vertex( (*it2).toVect(), COL_PH);
            k ++;       
          }  
          
          window.draw( polyhedra, l.size(), sf::Lines);  
    }
  }

//=========================================
//=========================================
void aff_edges(){
  
  
}
  
void draw_line(int i, int j){
  sf::Vertex edge_ij[] =
    {
      sf::Vertex( points[i].toVect(), sf::Color::Black),
      sf::Vertex( points[j].toVect(), sf::Color::Black)              
      // pour les SW :
    //  sf::Vertex( points[i].toVect(), e.color()),
    //  sf::Vertex( points[j].toVect(), e.color())              
   };
  window.draw( edge_ij, 2, sf::Lines);  
}


// ---------------------
// draw_multiple_line
// int i : indice du premier point
// int j : indice du second point
// int k : nombre de lignes à dessiner

void draw_multiple_line(int i, int j, int k){
  sf::Vector2f p1 = points[i].toVect();
  sf::Vector2f p2 = points[j].toVect();
  sf::Vector2f dir = (p2-p1);
  float dirx = dir.x;
  float diry = dir.y;
  // rotation 90degres
  dir.x = -diry;
  dir.y = dirx;  
  float norme_dir = sqrt((dir.x)*(dir.x) + (dir.y)*(dir.y));
  dir/= norme_dir;
  dir*= 5.f;

  for ( int d = 0 ; d < k ; d ++ ){
    sf::Vertex edge_ij[] =
      {
          sf::Vertex( p1 + dir*(float)d - dir*(float)(k/2.), sf::Color::Black),
          sf::Vertex( p2 + dir*(float)d- dir*(float)(k/2.), sf::Color::Black)              
     };
    window.draw( edge_ij, 2, sf::Lines);    
  }
}



// ----------------------------------
// draw_dashed_line

void draw_dashed_line(int i, int j){         
   sf::Vector2f p1 = points[i].toVect();
   sf::Vector2f p2 = points[j].toVect();
   float h = 10;
   float divi = 3;   
   
   sf::Vector2f dir = (p2-p1);
   float norme_dir = sqrt((dir.x)*(dir.x) + (dir.y)*(dir.y));
   dir/= norme_dir;
   
   for ( int d = 1 ; d <= divi ; d ++ ){
     sf::Vector2f pp = p1 + (p2-p1)*(d/(divi+1));
     sf::Vertex edge_ij[] =
    {
        sf::Vertex( pp, sf::Color::Black),
        sf::Vertex( pp + dir*h, sf::Color::Black)              
      
    };
     window.draw( edge_ij, 2, sf::Lines);  
   }
}


void affichage()
  {
  
  // affichage du mode de calcul automatique
  sf::Text text;
  text.setFont(font);
  text.setString(graph_mode_string);
  text.setCharacterSize(24); // exprimée en pixels, pas en points !
  text.setColor(sf::Color::Black);
  window.draw(text);

  // affichage du stretch
  text.setString( to_string(stretch()) );
  text.setPosition(0,25);
  window.draw(text);

  // remove mode
  if ( remove_mode )
    {
    text.setString( "remove mode" );
    text.setPosition(0,50);
    window.draw(text);
  }
  
  
  if ( graph_mode == GRAPH_MODE_TDD || graph_mode == GRAPH_MODE_W )
       if ( tool_delaunay_constructions )
        aff_polyedra2();
        
   if ( graph_mode == GRAPH_MODE_D2 )
       if ( tool_delaunay_constructions )
          aff_construction_Delaunay();     
     

  // affichage des arêtes
 list<Edge> edges = G.edges(); 
    for ( list<Edge>::iterator it = edges.begin() ; it != edges.end() ; ++ it )
      {
        Edge e = *it;
        int i = e.start().num();
        int j = e.end().num();
         
        if ( graph_mode != GRAPH_MODE_MOLEKULARIS )
          draw_line(i,j);
        else {
          if ( mol_liaisons[i][j] == 0 ){
            if ( a_donner[i] > 0 && a_donner[j] > 0 )
              draw_dashed_line(i,j);
          }
          else
            draw_multiple_line(i,j, mol_liaisons[i][j] );
        }
          
          
          
         
          
        
        // si on s'occupe du Schnyder wood  
        if (tool_SW && e.dir() != 0 ) // affichage des directions
          {
            if ( e.dir() == -1 )
              { swap(i,j); }
              
            coord u_ij ( points[j].x - points[i].x , points[j].y - points[i].y);
            coord milieu_ij ( (points[i].x + points[j].x)/2 , (points[i].y + points[j].y)/2 );

            coord w_ij ( - u_ij.y - u_ij.x , u_ij.x - u_ij.y );
            w_ij.normer();
            w_ij.x *= 10;
            w_ij.y *= 10;
            coord w2_ij ( - w_ij.y, w_ij.x );
            
            sf::Vertex arrow1[] =
            {                        
              sf::Vertex( milieu_ij.toVect(), sf::Color::Black),
              sf::Vertex( milieu_ij.toVect() - w_ij.toVect(), sf::Color::Black)
            };
            sf::Vertex arrow2[] =
            {            
              sf::Vertex( milieu_ij.toVect() - w2_ij.toVect(), sf::Color::Black),
              sf::Vertex( milieu_ij.toVect(), sf::Color::Black),           
            };
            
            window.draw( arrow1, 3, sf::Lines);  
            window.draw( arrow2, 3, sf::Lines);          
        }
      }
    
    
   aff_vertices();   
  }
  
  
  
  
  
// =======================================================  
// =======================================================
// COMBINATORIAL MAP

  
float testlol( int i, int j, int k)
  {
    coord u;
    u.x = points[j].x - points[k].x;
    u.y = points[j].y - points[k].y;
    u = rothoraire(u);
    return dotproduct( points[i], u );
  }
  
  
  // la carte combinatoire du graphe G
    vector<int> Gsigma;
     vector<int> Giota;
     vector< vector<int> > voisins;

// crée la carte combinatoire en demi arêtes du graphe
void graphe_comb_map2()
{  
  cout << "comb_map" << endl;
  list<Edge> E = G.edges();
  list<Vertex> V = G.vertices();
  int m = E.size();
  int n = V.size();
  vector< vector<bool> > adj(n); // matrice d'adjacence du graphe
 
  for ( int i = 0 ; i < n ; ++ i ) // init matrice adjacence
    {
      adj[i].resize(n);
      for ( int j = 0 ; j < n ; ++ j )
        adj[i][j] = false;
    }
    
  vector<int> es(m); // pour edge start
  vector<int> ee(m); // pour edge end
  Giota.resize(2*m);
  Gsigma.resize(2*m);
    
  // remplissage matrice d'adjacence
  int kk = 0;
  for( list<Edge>::iterator it = E.begin(); it != E.end(); it++ )
      {
        Edge e = *it;
        int x = e.start().num();
        int y = e.end().num();
        es[kk] = x;
        ee[kk] = y;
        adj[x][y] = true;
        adj[y][x] = true;        
        cout << "arete" << kk << " " << es[kk] << " " << ee[kk] << endl;
        Giota[2*kk] = 2*kk+1;
        Giota[2*kk+1] = 2*kk;
        kk++;
      }
  
  

voisins.resize(0);
voisins.resize(n);
// pour tout sommet v
for ( int v = 0 ; v < n ; ++ v )
  {
    int d = 0; // nombre de voisins vus = taille de voisins
    
    for ( int w = 0 ; w < n ; ++ w )    // on cherche les voisins de v
    if ( w != v && adj[v][w] )
      {
        if ( d < 2 ) // si moins de 1 voisins vus, on le rajoute direct
          {
            voisins[v].resize(d+1);
            voisins[v][d] = w;
            d ++;
          }
        else
          {
            bool is_w_set = false; // on regarde si w est entre voisins[i] et voisins[i+1]
          for ( int i = 0 ; i < d-1 ; ++ i) 
            {
              int a = voisins[v][i];
              int b = voisins[v][i+1];
              coord va;              
                va.x = points[a].x - points[v].x;
                va.y = points[a].y - points[v].y;
              coord vb;
                vb.x = points[b].x - points[v].x;
                vb.y = points[b].y - points[v].y;
              coord vw;
                vw.x = points[w].x - points[v].x;
                vw.y = points[w].y - points[v].y;
              
              if ( (dotproduct(rothoraire(va),vb) > 0 && dotproduct(rothoraire(va),vw) > 0 && dotproduct(rothoraire(vb),vw) < 0)
              || (dotproduct(rothoraire(va),vb) <= 0 && (dotproduct(rothoraire(va),vw) > 0 || dotproduct(rothoraire(vb),vw) < 0) ) )
                  { //le rajouter entre a et b
                    voisins[v].resize(d+1);
                    for ( int j = d-1 ; j >= i+1 ; j-- )
                      voisins[v][j+1] = voisins[v][j];
                    voisins[v][i+1] = w;
                    d++;                    
                    is_w_set = true; // on a trouvé où il était, on stop
                    break;                  
                  } 
             
            }
            if ( is_w_set == false ) // w n'était pas entre deux sommets, on le rajoute à la fin
              {
                voisins[v].resize(d+1);
                voisins[v][d] = w;
                d ++;
              }
          }
      
      }
      
    cout << "voisins de " << v << " : ";
    for ( int i = 0 ; i < d ; i ++ )
      cout << voisins[v][i] << " ";
    cout << endl;
    
    // on remplit Gsigma
    for ( int k = 0 ; k < m ; k ++ ) 
    if ( es[k] == v || ee[k] == v ) // pour toute arete incidente à v
      {
        int a = (es[k] == v ? ee[k] : es[k] ); // l'autre extremité de l'arête
        int demi_arete_a = (es[k] == v ? 2*k : 2*k+1);
        int b;
        for ( int i = 0 ; i < d ; i ++ )
          if ( voisins[v][i] == a )
          {
            b = ( i == d-1 ? voisins[v][0] : voisins[v][i+1] );
            break;
          }
        int demi_arete_b;
        for ( int kp = 0 ; kp < m ; kp ++ )
          if ( (es[kp] == b && ee[kp] == v) || (ee[kp] == b && es[kp] == v) )
            {
            demi_arete_b = (es[kp] == v ? 2*kp : 2*kp+1); 
            break;
            }

        Gsigma[demi_arete_a] = demi_arete_b;         
      }
  }
  
for(int i = 0 ; i < 2*m ; i ++ )
  {
    cout << "sigma(" << i << ") = "<< Gsigma[i] << endl;
  }
  

}
  
  

// next_anti(x,y) compute the neighbor of x following y in the anti clockwise order
// graphe_comb_map2 should have been called before in order to compute 'voisins'
int next_anti( int x, int y )
{
  vector<int> pile_voisins = voisins[x];
  int d = pile_voisins.size();
  for (int i = 0 ; i < d ; i ++ )
  {
    if ( pile_voisins[i] == y )
    {
      if ( i+1 < d )
        return pile_voisins[i+1];
      else
        return pile_voisins[0];
    }
  }
  
  return 0; // bug ?
}

int next( int x, int y )
{
  vector<int> pile_voisins = voisins[x];
  int d = pile_voisins.size();
  for (int i = 0 ; i < d ; i ++ )
  {
    if ( pile_voisins[i] == y )
    {
      if ( i-1 >= 0 )
        return pile_voisins[i-1];
      else
        return pile_voisins[d-1];
    }
  }
  
  return 0; // bug ?
}
  
  
// ======================
// L algo




#define INFTY 1000
list<int> Alist;
list<int> Blist;
vector<bool> tA;
vector<bool> tB;
vector<int> tAi; // le numéro ou INFTY
vector<int> tBi;

vector<int> NA;
vector<int> NB;


// initialize ?
void L_initialize(void)
{
  cout << "L_initialize()" << endl;
  int n = points.size();
  tA.clear();
  tB.clear();
  tA.resize(n);
  tB.resize(n);
  tAi.clear();
  tAi.resize(n);
  tBi.clear();
  tBi.resize(n);
  
  for ( int i = 0; i < n ; i ++ )
  {
    tA[i] = false;
    tB[i] = false;
    tAi[i] = INFTY;
    tBi[i] = INFTY;
  }

  int p = 0;
  for ( auto v : Alist )
  {
    p ++;
    tAi[v] = p;
    tA[v] = true;
  }  
  
  int q = 0;
  for ( auto v : Blist )
  {
    q++;
    tBi[v] = q;
    tB[v] = true;
  }

  NA.clear();
  NA.resize(n);
  NB.clear();
  NB.resize(n);     
 
  for ( int v = 0 ; v < n ; v ++ )
  {
    NA[v] = INFTY;
    NB[v] = INFTY;
    // :/ c'est du n^2 faudrait juste parcourir sur G.edges()
    if ( tA[v] == false && tB[v] == false )
    {
      for( auto e : G.edges() )
      {
        int x = e.start().num();
        int y = e.end().num();
        if ( x == v )
        {
          NA[v] = min ( NA[v], tAi[y] );
          NB[v] = min ( NB[v], tBi[y] );
        }
        if ( y == v )
        {
          NA[v] = min ( NA[v], tAi[x] );
          NB[v] = min ( NB[v], tBi[x] );
        }
      }
      
    }
  }
}



void L_inspect()
{
cout << "Inspect ---" << endl;
cout << "A : ";
for ( auto v : Alist )
	cout << v << " ";
cout << endl;
	cout << "B : ";
for ( auto v : Blist )
	cout << v << " ";
cout << endl;
cout << "a_p : " << Alist.back() << endl;
cout << "p   : " << Alist.size() << endl;
cout << "b_q : " << Blist.back() << endl;
cout << "q   : " << Blist.size() << endl;

for ( int v = 0 ; v < points.size() ; v ++ )
{
	cout << v << " " <<  (tA[v] ? (tB[v] ? "AB" : "A") : (tB[v] ? "B" : "-" ) ) << " " << (NA[v] == INFTY ? "#" : to_string(NA[v])) << " " << (NB[v] == INFTY ? "#" : to_string(NB[v])) << endl;
}


}





// L_system
// AL is a list of quintuple (v,x1,x2,y1,y2) : v is the vector number, (x1,y1) the coordinate of the bottom left corner, (x2,y2) of the upper right corner
// BL also for B vertices
// NL also for neutral vertices
// (x,y) is the translation vector for all L -> it is used in the cutting operation
// coord_A is the y-coord of the A vertices
// coord_B is the x-coord of the B vertices
class L_system{
  public:
    list<vector<int> > AL;
    list<vector<int> > BL;
    list<vector<int> > NL;
    int x;
    int y;
    int coord_A;
    int coord_B;
    
//    void push_back(vector<int> l){ coords.push_back(l); }
    void print(void);
    void save_svg(string s);
    int size(void);
};


int L_system::size(void)
{
  return AL.size() + BL.size() + NL.size();
}

// print a L_system
void L_system::print()
{
cout << "L_system.print()" << endl;
cout << x << " " << y << endl;
	for ( auto l : NL )
	{
		cout << l[0] << " N : " << l[1] -x << " " << l[2] -x << " " << l[3] -y << " " << l[4]-y << endl;
	}
		for ( auto l : AL )
	{
		cout << l[0] << " A : " << l[1]-x << " " << l[2]-x << " " << coord_A-y << " " << l[4]-y << endl;
	}
		for ( auto l : BL )
	{
		cout << l[0] << " B : " << coord_B-x << " " << l[2]-x << " " << l[3]-y << " " << l[4]-y << endl;
	}
	
}



// =============================
// save a L_system in a svg file
// string s : the file where to save the L_system
// todo : (x,y) translation to do

void L_system::save_svg(string s)
{
  ofstream file(s);
 file << "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>" << endl;
 file << "<svg> <g>" << endl;
 
 float epsilon = 0.04;
 float font_size = 0.8;
  
  for ( auto l : NL )
	{

		file << "<rect x=\"" << l[1] << "\"" << endl;
		file << "y=\"" << l[3] << "\"" << endl;
		file << "width=\"" << 1 - epsilon << "\"" << endl;
		file << "height=\"" << l[4]-l[3] - epsilon << "\"" << endl;
		file << "id=\"" << l[0] << "\"" << endl;
		file << "style=\"fill:#dadada;fill-opacity:1\"" << endl;
		file << "transform=\"scale(1,-1)\"" << endl;
		file << "/>" << endl;
		
	  file << "<rect x=\"" << l[1] << "\"" << endl;
		file << "y=\"" << l[3] << "\"" << endl;
		file << "width=\"" << l[2]-l[1] - epsilon << "\"" << endl;
		file << "height=\"" << 1 - epsilon << "\"" << endl;
		file << "id=\"" << l[0] << "\"" << endl;
		file << "style=\"fill:#dadada;fill-opacity:1\"" << endl;	
		file << "transform=\"scale(1,-1)\"" << endl;	
		file << "/>" << endl;
		
  	file << "<text x=\"" << l[1] << "\" y=\"" << -l[3] << "\" font-size=\"" << font_size << "\">" << l[0] << "</text>" << endl; 
		
	}
		for ( auto l : AL )
	{
		file << "<rect x=\"" << l[1] << "\"" << endl;
		file << "y=\"" << coord_A << "\"" << endl;
		file << "width=\"" << 1 - epsilon << "\"" << endl;
		file << "height=\"" << l[4]-coord_A - epsilon << "\"" << endl;
		file << "id=\"" << l[0] << "\"" << endl;
		file << "style=\"fill:#dadada;fill-opacity:1\"" << endl;	
		file << "transform=\"scale(1,-1)\"" << endl;	
		file << "/>" << endl;
		
	  file << "<rect x=\"" << l[1] << "\"" << endl;
		file << "y=\"" << coord_A << "\"" << endl;
		file << "width=\"" << l[2]-l[1] - epsilon << "\"" << endl;
		file << "height=\"" << 1 - epsilon << "\"" << endl;
		file << "id=\"" << l[0] << "\"" << endl;
		file << "style=\"fill:#dadada;fill-opacity:1\"" << endl;	
		file << "transform=\"scale(1,-1)\"" << endl;	
		file << "/>" << endl;
		
		file << "<text x=\"" << l[1] << "\" y=\"" << -coord_A << "\" font-size=\"" << font_size << "\">" << l[0] << "</text>" << endl; 
	}
		for ( auto l : BL )
	{
		file << "<rect x=\"" << coord_B << "\"" << endl;
		file << "y=\"" << l[3] << "\"" << endl;
		file << "width=\"" << 1 - epsilon << "\"" << endl;
		file << "height=\"" << l[4]-l[3] - epsilon << "\"" << endl;
		file << "id=\"" << l[0] << "\"" << endl;
		file << "style=\"fill:#dadada;fill-opacity:1\"" << endl;
		file << "transform=\"scale(1,-1)\"" << endl;		
		file << "/>" << endl;
		
	  file << "<rect x=\"" << coord_B << "\"" << endl;
		file << "y=\"" << l[3] << "\"" << endl;
		file << "width=\"" << l[2]-coord_B - epsilon << "\"" << endl;
		file << "height=\"" << 1 - epsilon << "\"" << endl;
		file << "id=\"" << l[0] << "\"" << endl;
		file << "style=\"fill:#dadada;fill-opacity:1\"" << endl;		
		file << "transform=\"scale(1,-1)\"" << endl;
		file << "/>" << endl;
		
  	file << "<text x=\"" << coord_B << "\" y=\"" << -l[3] << "\" font-size=\"" << font_size << "\">" << l[0] << "</text>" << endl; 
	}
	

  
  file << "</g> </svg>" << endl;
}





// L_decompo
// q is the length of Bl
// p is the length of Al
L_system  L_decompo( list<int> Bl, int q, list<int> Al, int p, int NIA, int NIB)
{


// init case when there are only 3 vertices
if ( p == 2 && q == 1)
{
  int a_2 = Al.back();
	Al.pop_back();
	int a_1 = Al.back();
	Al.push_back(a_2);
	int b_1 = Bl.back();
	
	if ( next_anti(b_1,a_2) == a_1 ) // A is of size 2 and B of size 1
	{
    L_system newL;		
		vector<int> La_1 = {a_1, 0,2,-2,0};
		vector<int> La_2 = {a_2, -2,0,-2,0};
		vector<int> Lb_1 = {b_1, -3,0,0,2};
		newL.AL.push_back( La_1 );
		newL.AL.push_back( La_2 );
		newL.BL.push_back( Lb_1 );
		newL.coord_A = -2;
		newL.coord_B = -3;
		newL.x = 0;
		newL.y = 0;
		return newL;
	}
}

if ( q == 2 && p == 1 )
{
	int a_1 = Al.back();
	int b_2 = Bl.back();
	Bl.pop_back();
	int b_1 = Bl.back();
	Bl.push_back(b_2);
	
if ( next_anti(b_2,a_1) == b_1 )
	{
    L_system newL;		
		vector<int> La_1 = {a_1, 0,2,-3,0};
		vector<int> Lb_1 = {b_1, -2,0,0,2};
		vector<int> Lb_2 = {b_2, -2,0,-2,0};
		newL.BL.push_back( Lb_1 );
		newL.AL.push_back( La_1 );
		newL.BL.push_back( Lb_2 );
		newL.coord_A = -3;
		newL.coord_B = -2;
		newL.x = 0;
		newL.y = 0;
		return newL;	
	}
}

int a_p = Al.back();
int b_q = Bl.back();

cout << "L_decompo()" << endl;
for ( auto av : Al )
	cout << av << " ";
cout << endl;
cout << "p   : " << p << endl;
for ( auto bv : Bl )
	cout << bv << " ";
cout << endl;
cout << "q   : " << q << endl;
cout << "NIA : " << NIA << endl;
cout << "NIB : " << NIB << endl;

// check for b_q removal
if ( NIA >= p && NA[b_q] >= p && q > 1 )
{
	cout << "B removal : " << b_q << " is deleted" << endl;
	int newNIA = INFTY;
	int newNIB = INFTY;
	Bl.pop_back(); // on enlève b_q de la liste
	int b_qm1 = Bl.back();
	
	int a_pp = a_p;
	int pp = p;
	
	int v = next_anti(b_q,a_p);
	int i = p+1;
	int vprev = a_p;
	
	while ( v != b_qm1 ) // on fait tous les voisisn de b_q
	{
		if ( tA[v] == false )
		{
			newNIB = INFTY;
			Al.push_back(v);
			a_pp = v;
			pp ++;
			tA[v] = true;
			if ( NB[v] == q)
				NB[v] = INFTY; // car b_q disparait et v n'était pas lié à un b_i, i < q
			
			int w = next_anti(v,vprev);
			while ( w != b_q ) // tous les voisins de v
			{
				NA[w] = min(NA[w],i);
				if ( tA[w] == false && tB[w] == false )
					newNIB = min( newNIB, NB[w]); // calcul en trop, il faut juste le faire pour a_p', mais c'est pas grave car il sera fait en dernier
				w = next_anti(v,w);
			}
		}
		vprev = v;
		v = next_anti(b_q,v);
		i ++;
	}
	
	// compute newNIA
	
	v = a_pp;
	int vfin;
	Bl.pop_back();
	if ( Bl.empty() )
		vfin = Al.front();
	else
		vfin = Bl.back();
	Bl.push_back(b_qm1);
	
	while ( v != vfin )
	{
		if ( tA[v] == false && tB[v] == false )
		{
			newNIA = min( newNIA, NA[v] );
		}
		v = next_anti(b_qm1,v);
	}
	
	
	
	cout << newNIA << endl;
	cout << newNIB << endl;
	
	L_system newL = L_decompo( Bl, q-1, Al, pp, newNIA, newNIB);
	
	// add the L of bq in the L_system without bq
	cout << "newL" << endl;
	newL.print();
	int u = newL.AL.back()[0];
	while ( u != a_p )
	{
		newL.AL.back()[3] = newL.coord_A;
		vector<int> l = newL.AL.back();
		newL.AL.pop_back();
		newL.NL.push_back(l);
		u = newL.AL.back()[0];
	}
	
	  newL.coord_B -= 1;
	vector<int> lb_q ={b_q,newL.coord_B, newL.AL.back()[1], newL.coord_A-1, newL.BL.back()[3]};
	newL.BL.push_back(lb_q);
	
	newL.coord_A -= 2;
	// regarder le ap removal pour voir ce qu'il y avait avant
	
	newL.print();
	int k = newL.size();
	newL.save_svg((string)"lol" + to_string(k) + (string)".svg");
		cout << "fin newL" << endl;
	return newL;
	
	
}


// check for A removal
if ( NIB >= q && NB[a_p] >= q && p > 1  )
{
	cout << "A removal : " << a_p << " is deleted" << endl;
	int newNIA = INFTY;
	int newNIB = INFTY;
	Al.pop_back();
	int a_pm1 = Al.back();
	
	int b_qq = b_q;
	int qq = q;
	
	int v = next(a_p,b_q);
	int i = q+1;
	int vprev = b_q;
	

	while ( v != a_pm1 ) // on fait tous les voisisn de a_p
	{
		if ( tB[v] == false )
		{
			newNIA = INFTY;
			Bl.push_back(v);
			b_qq = v;
			qq ++;
			tB[v] = true;
			if ( NA[v] == p)
				NA[v] = INFTY; // car a_p disparait
			
			int w = next(v,vprev);
			while ( w != a_p ) // tous les voisins de v
			{
				NB[w] = min(NB[w],i);
				if ( tA[w] == false && tB[w] == false )
					newNIA = min( newNIA, NA[w]); // calcul en trop, il faut juste le faire pour a_p', mais c'est pas grave car il sera fait en dernier
				w = next(v,w);
			}
		}
		vprev = v;
		v = next(a_p,v);
		i ++;
	}
	
	// compute newNIA
	
	v = b_qq;
	int vfin;
	Al.pop_back();
	if ( Al.empty() )
		vfin = Bl.front();
	else
		vfin = Al.back();
	Al.push_back(a_pm1);
	
	while ( v != vfin )
	{
		if ( tA[v] == false && tB[v] == false )
		{
			newNIB = min( newNIB, NB[v] );
		}
		v = next(a_pm1,v);
	}
	
	
	
	cout << newNIA << endl;
	cout << newNIB << endl;
	
	
	L_system newL = L_decompo( Bl, qq, Al, p-1, newNIA, newNIB);
	cout << "newLA" << endl;
	newL.print();
	int u = newL.BL.back()[0];
	while ( u != b_q )
	{
	  newL.BL.back()[1] = newL.coord_B;
		//newL.BL.back()[1] = newL.coord_B-1;
		vector<int> l = newL.BL.back();
		newL.BL.pop_back();
		newL.NL.push_back(l);
		u = newL.BL.back()[0];
	}

  newL.coord_A -= 1;
		vector<int> la_p ={a_p, newL.coord_B-1, newL.AL.back()[1], newL.coord_A, newL.BL.back()[3],};
		//		vector<int> la_p ={a_p, newL.coord_B-2, newL.AL.back()[1], newL.coord_A, newL.BL.back()[3],};
	newL.AL.push_back(la_p);

	newL.coord_B -= 2; // avant 3
	
  int k = newL.size();
	newL.print();
  newL.save_svg((string)"lol" + to_string(k) + (string)".svg");
		cout << "fin newLA" << endl;
	return newL;

}






// if impossible, then cutting
cout << "cutting" << endl;

// main part
int newNIA = INFTY;
int newNIB = INFTY;
int d = next_anti(b_q,a_p);
int i = NA[d];
int j = NB[d];

tA[d] = true; // d is supposed to be A in the main part;

// we start by removing the b_k vertices from Bl when k > j and put them in a list BBl
// which will be used in the B part
int v;
int newq  = q;
v = Bl.back();
list<int> BBl;
BBl.push_front(v);

while ( newq > j )
{
  Bl.pop_back();
  v = Bl.back();
  BBl.push_front(v);
  newq --;
}

// remove the a_k vertices from Al when k > i and put them in AAl
// AAl will be used in the A part
int newp = p;
v = Al.back();
list<int> AAl;
AAl.push_front(v);

while ( newp > i )
{
  Al.pop_back();
  v = Al.back();
  AAl.push_front(v);
  newp --;
}

int b_j = Bl.back();
int a_i = Al.back();


// every inner neighbor of d
v = Al.back(); 
while ( v != b_j )
{
	if ( tA[v] == false && tB[v] == false )
	{
		NA[v] = min(NA[v],i+1); // because d = a_{i+1}
		newNIB = min(newNIB, NB[v] );
	}
	v = next_anti(d, v);
}

// every inner neighbor of b_j
v = d;
int b_jm1 = Al.front();
if ( j > 1 )
{
  Bl.pop_back();
  b_jm1 = Bl.back();
  Bl.push_back(b_j);
}

while ( v != b_jm1 )
{
	if ( tA[v] == false && tB[v] == false )
	{
		newNIA = min ( newNIA, NA[v] );
	}
	v = next_anti(b_j, v);
}

Al.push_back(d);
L_system Lmain = L_decompo( Bl, j, Al, i+1, newNIA, newNIB );




// B part
// NB does not change
newNIA = INFTY;
newNIB = INFTY;
tA[d] = true;
tB[d] = false;

int w = next_anti(b_q,d);
if ( tB[w] == false ) // w can not be in A
	newNIA = 1;


v = b_j;
while (v != b_q )
{
	if ( tA[v] == false && tB[v] == false )
	{
		NA[v] = 1; // because v is adjacent to d
		newNIB = min ( newNIB, NB[v] );
	}
	v = next_anti(d,v);
}


L_system LB = L_decompo(BBl,q-j+1, {d},1, newNIA, newNIB);

// A part
// NA does not change
newNIA = INFTY;
newNIB = INFTY;
tA[d] = false;
tB[d] = true;

w = next(a_p,d);
if ( tA[w] == false ) // w can not be in A
	newNIB = 1;


v = a_i;
while (v != a_i )
{
	if ( tA[v] == false && tB[v] == false )
	{
		NB[v] = 1; // because v is adjacent to d
		newNIA = min ( newNIA, NA[v] );
	}
	v = next(d,v);
}


L_system LA = L_decompo({d},1,AAl,p-i+1, newNIA, newNIB);


// mix them Lmain, LB and LA
L_system newL = Lmain;

int k = newL.size();
	newL.print();
  newL.save_svg((string)"lol" + to_string(k) + (string)".svg");
  
return newL;









}
















// ==============================
// Stanchion & Penrose
 
  
  
  

// Given a combinatorial map (sigma & iota), compute the number of cycles when twisting the edges "twisting"

int nb_faces_under_twisting(vector<bool> twisting){
  int m = Giota.size();
  vector<int> Gisigma(m);
  for (int i = 0; i < m; ++i) 
    Gisigma[Gsigma[i]] = i;
    
	vector<bool> seen(m, false);
	int nbSeen = 0;

	queue<int> q;
	int nCycles = 0;
	while(nbSeen != m){
		// Find smallest not seen
		int i;
		for(i = 0; i < m && seen[i]; i++);
		q.push(i);
		
		// Explore from it
		while(!q.empty()){
			int x = q.front();
			q.pop();
			if(seen[x])
				continue;
			
			seen[x] = true;
			nbSeen++;

			// First neighboor		
			int z = (twisting[x])? Giota[x] : Gsigma[Giota[x]];
			if(!seen[z])
				q.push(z);
		
			// Second neighboor
			int y = Gisigma[x];
			z = (twisting[y])? Gsigma[Giota[y]] : Giota[y];
			if(!seen[z])
				q.push(z);	
		}
		
		nCycles++;
	}	
	
	return nCycles;
}


// compute the graph of the MSSs
void MSS_space(){

 cout << "MSS space : "<< endl;
  list<Edge> E = G.edges();
  list<Vertex> V = G.vertices();
   int m = E.size();
  int n = V.size();
  int f = 2-n+m;
 
 int nMSS = 0;
 vector< vector<bool> > good_twistings;

 // Construct the list of edges (of size n/2)
	vector<int> edges;
	for(int x = 0; x < 2*m; x++)
	if(x < Giota[x])
		edges.push_back(x);
		
	
	// Iterate over all possible choices (2^(n/2))
	long_t result = 0;
	long_t x = 0;
	long_t k = 1ll << (m);
	vector<bool> twisting(2*m, false);
	for( ; x < k; x++){
		// Compute the corresponding twisting
		long_t y = x;
		for(int i = 0; i < m; i++){
			twisting[edges[i]] = twisting[Giota[edges[i]]] = ((y % 2) != 0);
			y /= 2;
		}
		
		// Test it
		int nf = nb_faces_under_twisting(twisting);
	
		
	if ( nf == 1 ){
	  nMSS ++;
	  good_twistings.resize( nMSS);
	  good_twistings[nMSS-1].resize(2*m);
		  //cout << "MSS : " ;
		  for ( int i = 0 ; i < 2*m ; i ++ ){
		    good_twistings[nMSS-1][i] = twisting[i];
		    //cout << twisting[i] << " ";
		  }
		  //cout << endl;
		}
	}
	
	cout << nMSS << endl;
	
	// ça c'était pour calculer le nombre de compo connexes de l'espace des solutions
	/*
	 cout << "calcul du MSS space" << endl;
 Graph MSSspace = Graph();
 for (int i = 0 ; i < nMSS ; i ++ ) 
  MSSspace.add_vertex(i);
  
 vector< vector<bool> > adj;
 adj.resize(nMSS);
 for ( int i = 0 ; i < nMSS ; i ++ )
  adj[i].resize(nMSS);
  
 for ( int i = 1 ; i < nMSS; i ++ )
  for ( int j = 0 ; j < i ; j ++ ){
    int ndiff = 0;
    for ( int k = 0 ; k < 2*m ; k ++ )
      if ( good_twistings[i][k] != good_twistings[j][k] )
        ndiff ++;
    
    if ( ndiff <= 4 ){
      MSSspace.add_edge(i,j);  
      adj[i][j] = true;
      adj[j][i] = true;
    }
  }
  cout << "calculer nb compo connexes" << endl;
  MSSspace.sage_format("mss.txt");
  cout << MSSspace.number_connected_components() << endl;
  
  cout << MSSspace.nbcc(adj) << endl;
  */
    

}

void Penrose_polynomial()
  {
    cout << "Penrose polynomial : "<< endl;
  list<Edge> E = G.edges();
  list<Vertex> V = G.vertices();
   int m = E.size();
  int n = V.size();
  int f = 2-n+m;
  vector<int> coeff(f+1,0);
  
    // Construct the list of edges (of size n/2)
	vector<int> edges;
	for(int x = 0; x < 2*m; x++)
	if(x < Giota[x])
		edges.push_back(x);
		
	
	// Iterate over all possible choices (2^(n/2))
	long_t result = 0;
	long_t x = 0;
	long_t k = 1ll << (m);
	vector<bool> twisting(2*m, false);
	for( ; x < k; x++){
		// Compute the corresponding twisting
		long_t y = x;
		for(int i = 0; i < m; i++){
			twisting[edges[i]] = twisting[Giota[edges[i]]] = ((y % 2) != 0);
			y /= 2;
		}
		
		// Test it
		int size_twisting = 0;
		for ( int i = 0 ; i < 2*m ; i ++ ){
		  if (twisting[i] )
		    size_twisting ++;
		}
		size_twisting /= 2;
		 		
		int nf = nb_faces_under_twisting(twisting);
	
		
		if (size_twisting % 2 == 0 )
		  coeff[nf] ++;
		else
		  coeff[nf] --;
		
	}
	
	for ( int i = 0 ; i <= f ; i ++ )
	  cout << coeff[i] << " * " << "x^" << i << " + ";
	cout << endl;
	  
  }
  


	

  
  
  
  
  
  
  
  

  /*
void graphe_dual()
  {
  
  cout << "dual" << endl;
  list<Edge> E = G.edges();
  list<Vertex> V = G.vertices();
  int m = E.size();
  int n = V.size();
  vector<int> es(m);
  vector<int> ee(m);
  vector<int> iota(2*m);
  vector<int> sigma(n);

  int kk = 0;
  for( list<Edge>::iterator it = E.begin(); it != E.end(); it++ )
      {
        Edge e = *it;
        es[kk] = e.start().num();
        ee[kk] = e.end().num();
        cout << "arete" << kk << " " << es[kk] << " " << ee[kk] << endl;
        iota[2*kk] = 2*kk+1;
        iota[2*kk+1] = 2*kk;
        kk++;
      }
      
        
  
  
    
  
for ( int i = 0 ; i < n ; i ++ )
  {
    for ( int k = 0 ; k < m ; k ++ )
      {
        if (ee[k] == i || es[k] == i)
          {
            for ( int kp = 0 ; kp < m ; kp ++ )
            if ( kp != k )
              {
                bool b = true;
                for ( int w = 0 ; w < m ; w ++ )
                if ( w != k && w != kp )
                  {
                    if ( testlol(w,kp,i) < 0 && testlol(w,k,i) > 0 )
                      {b = false; break;}
                  }
                if ( b )
                  {
                      sigma[es[k] == i ? 2*k : 2*k+1] = (es[kp] == i ? 2*kp : 2*kp +1 );
                  }              
              }
          }
      
      }
  }
  
for(int i = 0 ; i < 2*m ; i ++ )
  {
    cout << "sigma(" << i << ") = "<< sigma[i] << endl;
  }
  
  // calculer les cycles de sigma iota
//  rajouter les points barycentriques

vector<bool> tvu(2*m);
for ( int i = 0 ; i < 2*m ; i ++ )
  tvu[i]= false;

        cout << "ko" << endl;
        
for ( int k = 0 ; k < 2*m ; k ++ )
  {
    cout << "-" << endl;
    if ( tvu[k] == false )
      {

        tvu[k] = true;
        coord somme;
        if (k %2 == 0 )
          {
          somme = points[es[k/2]];
          }
        else
          {
          somme = points[ee[k/2]];
          }
        int w = iota[sigma[k]];
        int len = 1;
        while ( w != k )
          {
            len ++;
            if ( w%2 == 0 )
              {
                somme.x = somme.x + points[es[k/2]].x;
                somme.y = somme.y + points[es[k/2]].y;
              }
            else
              {
                somme.x = somme.x + points[ee[k/2]].x;
                somme.y = somme.y + points[ee[k/2]].y;              
              }
            w = iota[sigma[k]];
          }
        somme.x = somme.x / len;
        somme.y = somme.y / len;
        points.push_back( somme );
        int nn = points.size();
        G.add_vertex(nn-1);
      }  
  }
  
  
  
  }
  */
  
  



/*

HANDSHAKING PROBLEM

calcul le planar line graph :
sommets = arêtes fictives entre chaque point
u et v reliés si même endpoints ou croisement des segments

*/  
Graph handshaking()
  {
    int n = points.size();

    Graph HS = Graph((n*(n-1))/2);
    int m = ((n*(n-1))/2);
    vector< vector<int> >  aretes (m);

   
    int k = 0;    
    for (int i = 0 ; i < n ; ++ i )
    for (int j = i+1 ; j < n ; ++ j )
      {
        aretes[k].resize(2);
        aretes[k][0] = i;
        aretes[k][1] = j;
        k++;
      }
    
    
    for (int u = 0; u < m ; ++ u )
    for ( int v = u+1 ; v < m ; ++ v )
      {
        int u0 = aretes[u][0];
        int u1 = aretes[u][1];
        int v0 = aretes[v][0];
        int v1 = aretes[v][1];
        
        if ( u0 == v0 || u0 == v1 || u1 == v0 || u1 == v1 )
          HS.add_edge(u+1,v+1);
        else
          {
            float a = points[u0].x - points[u1].x;
            float b = points[v0].x - points[v1].x;
            float c = points[u0].y - points[u1].y;
            float d = points[v0].y - points[v1].y;
            float det = a*d-b*c;
            
            if (det != 0 )
              {
                float x1 = points[v0].x - points[u1].x;
                float x2 = points[v0].y - points[u1].y;
                
                float t1 = (a*x2-c*x1)/det;
                float t2 = (d*x1-b*x2)/det;
                if ( 0 <= t1 && t1 <= 1 && 0 <= t2 && t2 <= 1 )
                  HS.add_edge(u+1,v+1);
              }
            
          }
          
      }
    
    
    
    HS.sage_format();
    int mlg = (HS.edges()).size();
    cout << mlg << endl;
    cout << n << endl;
    cout << mlg - (n*(n-1)*(n-2))/2 << endl;
    return HS;
  }
  
  
int a(int i)
  {
  if ( i == 0 )
    return 0;
  if ( i == 1 )
    return 0;
  if ( i == 2 )
    return 1;
  if ( i == 3 )
    return -1;
  
  return 4*a(i-2) - 2*a(i-3);
  }


coord rot ( float alpha , coord p, coord c )
  {
  return coord( c.x + (p.x-c.x)*cos(alpha) - (p.y-c.y)*sin(alpha) , c.y + (p.y-c.y)*cos(alpha) + (p.x-c.x) *sin(alpha));  
  }
  


void Jensen( int r )
  {
  G = Graph();
   points.resize(3*r+3);
   float alpha = PI/6;
   float Gx = 500;
   float Gy = 500;
   float h = 400;
   float l = sqrt(3)*h;
   
   float stepx = 50;
   float stepy = 20;
   
   float Ax = Gx;
   float Ay = Gy-h;
   float By = Gy + h/2;
   float Bx = Gx + l/2;
   float Cx = Gx - l/2;
   float Cy = By;
   
   points[3*r].x = Ax;
   points[3*r].y = Ay;
   points[3*r+1].x = Bx;
   points[3*r+1].y = By;
   points[3*r+2].x = Cx;
   points[3*r+2].y = Cy;
   
   
   for ( int i = 0 ; i < r ; i ++ )
    {
      points[i].x = Bx - (i+1)*stepx;
      points[i].y = By + a(i+1)*stepy;  
      points[i] = rot(  alpha  , points[i], points[3*r+1]);
        
      points[i+r].x = Cx - (i+1)*stepx;
      points[i+r].y = Cy + a(i+1)*stepy;  
       points[i+r] = rot( 2*PI/3 + alpha  , points[i+r], points[3*r+2]);
       
       points[i+2*r].x = Ax - (i+1)*stepx;
       points[i+2*r].y = Ay + a(i+1)*stepy;  
       points[i+2*r] = rot( 4*PI/3 + alpha  , points[i+2*r], points[3*r]);
      
    }
    
   
   
   
   
   cout << "lol" << endl;
  
  
  }
  
  

   float fJ( int i, float mu )
  {
  return (pow(2,i)-1) *mu;
  }
  
void JensenNew( int r )
  {
  G = Graph();
   points.resize(3*r);
   

   
   float alpha = PI/6;
   float Gx = 500;
   float Gy = 500;
   float h = 400;
   float l = sqrt(3)*h;
   
   float x = h/2;   
  float mu = x*tan(PI/20)/(pow(2,r-1)-1);
   
   float stepx = h/(2*r);

   
   float Ax = Gx;
   float Ay = Gy-h;
   float By = Gy + h/2;
   float Bx = Gx + l/2;
   float Cx = Gx - l/2;
   float Cy = By;
   
   coord A(Ax,Ay);
   coord B(Bx,By);
   coord C(Cx,Cy);
   
  
   
   
   for ( int i = 0 ; i < r ; i ++ )
    {
      points[i].x = Bx - i*stepx;
      points[i].y = By + fJ(i,mu);  
      points[i] = rot(  2*PI/9  , points[i], B);
      
      
      points[i+r].x = Cx - i*stepx;
      points[i+r].y = Cy + fJ(i,mu);
       points[i+r] = rot( 2*PI/3 + 2*PI/9  , points[i+r], C);
       
       points[i+2*r].x = Ax - i*stepx;
       points[i+2*r].y = Ay + fJ(i,mu);
       points[i+2*r] = rot( 4*PI/3 + 2*PI/9  , points[i+2*r], A);
      
    }
    
   
   
   
   
   cout << "lol" << endl;
  
  
  }
  
  
  
  
  
#include"src/moleku.cpp"


void mol_init(){
  int n = points.size();
  
  mol_phi.resize(n);
  for ( int i = 0 ; i < n ; i ++ )
    mol_phi[i] = 1;
  
  mol_liaisons.resize(n);
  for ( int i = 0 ; i < n ; i ++ ){
    mol_liaisons[i].resize(n);
    for ( int j = 0 ; j < n ; j ++ )
      mol_liaisons[i][j] = 0;
    }


}
   







#include"src/save.cpp"



/*
A
B handshaking : calcul
C handshaking ??
D (met les sommets en position ...)
E efface le graphe en entier
F
G (met les sommets en position ...)
H (met les sommets en position ...)
I
J met les sommets en position Jensen
K crée un clique avec tous les points
L load graph
M change de mode
N nbcc compo connexe
O
P (met les sommets en position ...)
Q
R remove mode
S sauvegarde le graphe
T Penrose polynomial
U resolution de molekularis
V reinitialise les liaisons de molekularis
W
X
Y
Z

*/


  
  

int main(){  
  font.loadFromFile("nevis.ttf");
  graph_mode = 0;
  init_graph_mode_string();
  aff_init_text_vertex();
  
  shapeVertex.setFillColor(sf::Color::Black);

  while (window.isOpen()){
    sf::sleep(sf::milliseconds(3));
    sf::Event event;
    while (window.pollEvent(event))
    {
        // check the type of the event...
          switch (event.type)
          {
              // window closed
              case sf::Event::Closed:
                  window.close();
                  break;
                  
              case sf::Event::KeyPressed:
              
                  if (event.key.code == sf::Keyboard::Escape)
                    window.close();
                  else if (event.key.code == sf::Keyboard::M)
                    {
                    graph_mode += 1;
                    graph_mode = graph_mode % GRAPH_MODE_TOTAL;
                    init_graph_mode_string();
                    
                    if ( graph_mode == GRAPH_MODE_MOLEKULARIS ){
                      mol_init();                    
                    }
                    
                    if ( graph_mode == GRAPH_MODE_TDD )
                      {
                        base[0].x = 0;
                        base[0].y = -1;
  
                        base[1].x = 0.86602;
                        base[1].y = 0.5;
                        
                        base[2].x = -0.86602; // cos(2pi/3 + pi/2)
                        base[2].y = 0.5;
                      }
                    else if ( graph_mode == GRAPH_MODE_W ) 
                      {                        
                        base[0].x = 0;
                        base[0].y = -1;
                        
                        base[1].x = -1;
                        base[1].y = 0.5;                        
                        
                        base[2].x = 1.2;
                        base[2].y = 0.;                    
                      }
                      
                    
                    computeGraph();   
                    }
                  // L algo
                  else if ( event.key.code == sf::Keyboard::F )
                  {
                  	graphe_comb_map2();
                    
                    Alist = {2,3};
                    Blist = {0,1};
                    L_initialize();
                  }
                  else if ( event.key.code == sf::Keyboard::Q)
                  {                    
                    int b_q = Blist.back();
                    int a_p = Alist.back();

                    int NIA = INFTY;
                    int NIB = INFTY;
                    
                    for ( auto e : G.edges() )
                    {
                      int x = e.start().num();
                      int y = e.end().num();
                      if ( x == b_q && tA[y] == false && tB[y] == false )
                        NIA = min (NIA, NA[y]);
                      if ( y == b_q && tA[x] == false && tB[x] == false )
                        NIA = min (NIA, NA[x]);
                      if ( x == a_p && tA[y] == false && tB[y] == false )
                        NIB = min (NIB, NB[y]);
                      if ( y == a_p && tA[x] == false && tB[x] == false )
                        NIB = min (NIB, NB[x]);
                    }
                    
                    
                    
                    L_system L = L_decompo(  Blist, Blist.size(), Alist, Alist.size(), NIA,NIB );
                    
                    L.print();
                  
                  }
                  else if (event.key.code == sf::Keyboard::I)
                  {
                  L_inspect();
                  }
                  else if (event.key.code == sf::Keyboard::B)
                    {
                    Graph HS = handshaking();
                    int n = points.size();
                    
                    HS.canColor(n-1) ;                      
                    }
                     else if (event.key.code == sf::Keyboard::J)
                    {
                      JensenNew(7);
                      computeGraph();
                    }
                    else if ( event.key.code == sf::Keyboard::N ){
                  //cout << G.nbcc() << endl;
                 }
                  else if (event.key.code == sf::Keyboard::T)
                    {
                      cout << "lol"<< endl;
                      graphe_comb_map2();
                      //Penrose_polynomial();
                      MSS_space();
                    }
                  else if (event.key.code == sf::Keyboard::R){
                    remove_mode = !remove_mode;
                  }
                  else if (event.key.code == sf::Keyboard::S){
                  	if ( graph_mode == GRAPH_MODE_MOLEKULARIS )
                  		save_moleku();
                  	else
                  		save_graph();
                  		
                    G.sage_format();
                  }
                  else if ( event.key.code == sf::Keyboard::L ){
                    
                    if ( graph_mode == GRAPH_MODE_MOLEKULARIS )
                  		load_moleku("graphs/lol.moleku");
                  	else
                  		load_graph("graphs/lol.graph");
                  }
                  else if ( event.key.code == sf::Keyboard::U ){
                    solve_moleku();
                  }
                  else if ( event.key.code == sf::Keyboard::V ){
                    init_graphe(points.size());
                    init_aux(points.size());
                  }
                  else if (event.key.code == sf::Keyboard::C)
                    {
                      handshaking();
                    }
                    else if (event.key.code == sf::Keyboard::K)
                    {
                      int n = G.vertices().size();
                      for ( int i = 0 ; i < n ; i ++ )
                      for ( int j = i +1 ; j < n ; j ++ )
                        if ( G.is_edge(i,j) == false)
                          G.add_edge(i,j);
                    }
                  else if (event.key.code == sf::Keyboard::E)
                    {
                    points.resize(0);
                    G = Graph();
                    computeGraph();
                    }
                  else if (event.key.code == sf::Keyboard::P)
                    {
                    putLp();
                    computeGraph();
                    }
                  else if (event.key.code == sf::Keyboard::G)
                    {
                    N ++;
                    putLp();
                    computeGraph();
                    }
                  else if (event.key.code == sf::Keyboard::H)
                    {
                    pp ++;
                    putLp();
                    computeGraph();
                    }
                  else if (event.key.code == sf::Keyboard::D)
                    {
                    putOcto();
                    computeGraph();
                    
                    }
              

                  break;
                  
              case sf::Event::MouseButtonReleased:
                  current_point = -1;
                break;
              
              case sf::Event::MouseMoved:
                if (current_point != -1 )
                  {
                    int x = event.mouseMove.x;
                    int y = event.mouseMove.y;
                    
                    points[current_point].x = x;
                    points[current_point].y = y;
                    computeGraph();                    
                  }
                  
                break;

              case sf::Event::MouseButtonPressed:  
                // Clic Gauche : on déplace le point sélectionnée s'il y en a un            
                if (event.mouseButton.button == sf::Mouse::Left){
                  int x = event.mouseButton.x;
                  int y = event.mouseButton.y;
                  int i = isPointNear( x,y);                  
                  if ( i >= 0 ){
                    current_point = i;
                  }
                  
                  sf::Vector2i e = clic_on_edge(x,y);
                    if ( e.x >= 0 ){
                      if ( graph_mode == GRAPH_MODE_MOLEKULARIS ){
                        if (a_donner[e.x] >0 && a_donner[e.y]>0){
                          add_liaison(e.x , e.y , points.size());
                        }
                      }
                    }
                      
                      
                    
                  
                }
                // Clic Droit
                  else{           
                    int x = event.mouseButton.x;
                    int y = event.mouseButton.y;
                           
                    // si on est mode molekularis
                    sf::Vector2i e = clic_on_edge(x,y);
                    if ( e.x >= 0 ){
                      if ( graph_mode == GRAPH_MODE_MOLEKULARIS ){
                        if ( mol_liaisons[e.x][e.y] > 0 ){
                          remove_liaison(e.x , e.y , points.size());
                        }
                      }
                    }

                    int i = isPointNear(x,y);
                    if (i >= 0){ // un sommet a été sélectionné
                      
                      if ( graph_mode == GRAPH_MODE_MOLEKULARIS ){
                        mol_phi[i] ++;
                        if (mol_phi[i] == 5 )
                          mol_phi[i] = 1;                      
                      }                      
                      else if ( remove_mode ){                          
                        for ( int j = i +1 ; j < points.size() ; ++ j )
                          {
                            points[j-1] = points[j];
                          }
                        points.resize( points.size() -1 );
                        
                        G.remove_vertex(i);
                        cout << i << endl;
                      }
                      else
                        {
                      
                        cout << i << "selected" << endl;
                        if (current_add_edge_point >= 0 ) // rajouter l'arete
                          {
                            cout << current_add_edge_point << " was selected" << endl;
                            
                            if ( G.is_edge(i,current_add_edge_point) )
                              G.remove_edge(i,current_add_edge_point);
                            else
                              G.add_edge(i,current_add_edge_point);
                            current_add_edge_point = -1;
                          }
                        else // un premier sommet à été sélec
                          current_add_edge_point = i;
                        }
                      }
                    else if ( graph_mode != GRAPH_MODE_MOLEKULARIS )// on crée un nouveau somet si on est pas en mode molekularis                     
                      {                    
                        coord new_point;
                        new_point.x = x;
                        new_point.y = y;
                        points.push_back( new_point );
                        if ( graph_mode == GRAPH_MODE_A_LA_MAIN )
                          {
                            int n = points.size();
                            G.add_vertex(n-1);
                            computeGraph();
                          }
                        else
                          computeGraph();
                      }
                  }
                  
                break;

              default:
                  break;
          }
    
    
        }

        window.clear(sf::Color::White);
        affichage();
        window.display();
    }

    return 0;
}
