

// test connexe        
// moyen de le faire inteligement ? genre s'il y a un H on sait que ça passera pas : virer les sommets comme








// =====================================================================
// =====================================================================


// affichage des tableaux
void print(int n){
  cout << "  mat adj :" << endl;
  for ( int i = 0 ; i < n ; i ++ ){
    for ( int j = 0 ; j < n ; j ++ )
      cout << m[i][j] << " ";
    cout << endl;
    }
  
  cout << "  mol_phi :" << endl;
  for ( int i = 0 ; i < n ; i ++ )
    cout << mol_phi[i] << " ";
  cout << endl;

  cout << "  a_donner :" << endl;
  for ( int i = 0 ; i < n ; i ++ )
    cout << a_donner[i] << " ";
  cout << endl;
  
  cout << "  a_recuperer :" << endl;
  for ( int i = 0 ; i < n ; i ++ )
    cout << a_recuperer[i] << " ";
  cout << endl;
  
  cout << "  mol_liaisons :" << endl;
  for ( int i = 0 ; i < n ; i ++ ){
    for ( int j = 0 ; j < n ; j ++ )
      cout << mol_liaisons[i][j] << " ";
    cout << endl;
    }


}

// initialise les tableaux pour un graphe à n sommets
// ! suppose que mol_phi n'a pas été donné donc ne rempli pas à donner et a_recuperer !
void init_graphe(int n){
  m.resize(n);
  for ( int i = 0 ; i < n ; i ++ ){
    m[i].resize(n);
    for ( int j = 0 ; j <n ; j ++ )
      m[i][j] = G.is_edge(i,j);
    }
  
  a_donner.resize(n);
  a_recuperer.resize(n);
  for( int i = 0 ; i < n ; i ++ ){
    a_donner[i] = 0;
    a_recuperer[i] = 0;
  }
  
  mol_liaisons.resize(n);
  for ( int i = 0 ; i < n ; i ++ ){
    mol_liaisons[i].resize(n);
    for ( int j = 0 ; j < n ; j ++ )
      mol_liaisons[i][j] = 0;
    }
}



// initialise a_donner et a_recuperer à partir de mol_phi
void init_aux(int n){
  for ( int i = 0 ; i < n ; i ++ ){
    a_donner[i] = mol_phi[i];
    a_recuperer[i] = 0;
    for ( int j = 0 ; j < n ; j ++ )
      if ( m[i][j] )
        a_recuperer[i] += mol_phi[j];
  }    
}



// =====================================================================
// =====================================================================


void add_liaison( int i , int j, int n ){
  a_donner[i] --;
  a_donner[j] --;
  mol_liaisons[i][j] ++;
  mol_liaisons[j][i] = mol_liaisons[i][j];
  for ( int k = 0 ; k < n ; k ++ ){
    if ( m[k][i] )
      a_recuperer[k] --;
    if ( m[k][j] )
      a_recuperer[k] --;
  }
}

void remove_liaison( int i , int j, int n ){
  a_donner[i] ++;
  a_donner[j] ++;
  mol_liaisons[i][j] --;
  mol_liaisons[j][i] = mol_liaisons[i][j];
  for ( int k = 0 ; k < n ; k ++ ){
    if ( m[k][i] )
      a_recuperer[k] ++;
    if ( m[k][j] )
      a_recuperer[k] ++;
  }
}





// =====================================================================
// =====================================================================


// renvoie -1 si pas de sommets saturable
// renvoie un sommet saturable sinon
int saturables(int n){
  for ( int i = 0 ; i < n ; i ++ )
    if ( a_donner[i] == a_recuperer[i] && a_donner[i] != 0 )
      return i;
  
  return -1;
}

// saturer :
// int i est le sommet à saturer (tq a_donner == a_recuperer)
// sortie : la liste des sommets avec qui une liaison a été créée (avec répétitions)
list<int> saturer(int i, int n){
  list<int> l;
  for ( int j = 0 ; j < n ; j ++ )
    if ( m[i][j] ){
      int nl = a_donner[j];
      for ( int k = 0 ; k < nl ; k ++ ){
        add_liaison(i,j,n);
        l.push_back(j);
      }
    }
  return l;
}

void desaturer(int i, list<int> l, int n){
  for( int j : l )
    remove_liaison(i,j,n);
}



// =====================================================================
// =====================================================================
// test de connexité

// a améliorer : un H ne permet pas de connecter deux composantes
bool test_connexite(int n){
  vector<bool> vu(n);
  vector<bool> deja_rajouter_a_traiter(n);
  list<int> a_traiter;
  int nb_a_voir = n;
  
  for ( int i = 0 ; i < n ; i ++ ){
      vu[i] = false;
      deja_rajouter_a_traiter[i] = false;
      
    if ( a_traiter.empty() && mol_phi[i] != 1 )
      a_traiter.push_back(i);
  }
  
  while ( !a_traiter.empty() ){
    int i = a_traiter.back();
    a_traiter.pop_back();
    
    vu[i] = true;
    nb_a_voir --;
    
    for ( int j = 0 ; j < n ; j ++ )
      if (  vu[j] == false && m[i][j] ){
        if ( mol_liaisons[i][j] > 0 ){
          a_traiter.push_back(j);
          vu[j] = true;
        }
        else if ( a_donner[i] != 0 && a_donner[j] != 0 ){
          a_traiter.push_back(j);      
          vu[j] = true;
        }
      }
  }

  if ( nb_a_voir == 0 )
    return true;
  else
    return false;  
}




// =====================================================================
// =====================================================================
// resolution par backtracking





bool resolution(int n){
  int i = -1;

  /*  
  cout << "------------------------------------" << endl;
  window.clear(sf::Color::White);
  affichage();
  window.display();
  print(n);
  
  sf::sleep(sf::milliseconds(1000));
  */
  
  for ( int v = 0 ; v < n ; v ++ )
    if ( a_donner[v] != 0 ){
      i = v;
      break;
    }
    
  // si tous les sommets sont saturés on a gagné
  if ( i == -1 )
    return true;
    
  // s'il y a un sommet saturable on le sature et on relance
  int u = saturables(n);
  if ( u != -1 ){  
    cout << "saturation : " << u << endl;
    list<int> l = saturer(u,n);
    if ( test_connexite(n) )
      if ( resolution(n) )
        return true;           
    desaturer(u,l,n);    
    return false;
  }
  
  
  // trouver le sommet qui a le moins a donner
  for ( int v = 0 ; v < n ; v ++ )
    if ( a_donner[v] != 0 && a_donner[v] < a_donner[i] )
      i = v;

  // augmenter de 1 une de ses mol_liaisons
  for ( int j = 0 ; j < n ; j ++ )
    if ( m[i][j] && a_donner[j] != 0 ){
      add_liaison(i,j,n);
      if ( test_connexite(n) )
        if ( resolution(n) )
          return true;
      remove_liaison(i,j,n);
    }

  
  return false;
}

// ===========================
// number of solution

// suppose that connectivy ok
int nb_solution(int n){
  int i = -1;

  
  for ( int v = 0 ; v < n ; v ++ )
    if ( a_donner[v] != 0 ){
      i = v;
      break;
    }
    
  // si tous les sommets sont saturés on a gagné
  if ( i == -1 )
    return 1;
    
  int sum =0;
    
  // s'il y a un sommet saturable on le sature et on relance
  int u = saturables(n);
  if ( u != -1 ){  
    cout << "saturation : " << u << endl;
    list<int> l = saturer(u,n);
    if ( test_connexite(n) )
      sum = nb_solution(n);    
    desaturer(u,l,n);    
    return sum;
  }
  
  
  // trouver le sommet qui a le moins a donner
  for ( int v = 0 ; v < n ; v ++ )
    if ( a_donner[v] != 0 && a_donner[v] < a_donner[i] )
      i = v;

  // augmenter de 1 une de ses mol_liaisons

// FAIRE TOUS LES CAS !!!!!
  
  for ( int j = 0 ; j < n ; j ++ )
    if ( m[i][j] && a_donner[j] != 0 ){
      add_liaison(i,j,n);
      if ( test_connexite(n) )
        sum += nb_solution(n);    
          
      remove_liaison(i,j,n);
    }

  
  return sum;
}




// ===========================

bool solve_moleku(){
  int n = points.size();
  init_graphe(n);
  init_aux(n);
  
  cout << " --- " << endl;
 cout << resolution(n) << endl;
    cout << " --- " << endl;

}




