


void save_moleku(){
  ofstream file("graphs/lol.moleku");
  int n = points.size();
  // nombre de sommets
  file << n << endl;
 
  // position géométrique puis type molecule
  for ( int i = 0 ; i < n ; i ++ ){
    file << points[i].x << endl;
    file << points[i].y << endl;
    file << mol_phi[i] << endl;
  }
  
  // adjancence
  for ( int i = 0 ; i < n ; i ++ )
    for ( int j = 0 ; j < n ; j ++ )
      file << G.is_edge(i,j) << endl;  
}


void load_moleku( string s ){
  ifstream file(s);
  
  int n;
  file >> n;
  points.resize(n);  
  G = Graph();
  mol_phi.resize(n);
  
  init_graphe(n);
  init_aux(n);

      
  for ( int i = 0 ; i < n ; i ++ ){
    G.add_vertex(i);
    file >> points[i].x;
    file >> points[i].y;
    file >> mol_phi[i];
  }
  
  
  for ( int i = 0 ; i < n ; i ++ )
    for ( int j = 0 ; j < n ; j ++ ){
      bool b;
      file >> b;
      if ( b && i < j )      
        G.add_edge(i,j);
    }
    
   remplirA();
    FloydWarshall();
      

}



// =============================
// save_geometric_graph : save what you see on the screen but in svg format
// todo
// use aff_construction_Delaunay and  aff_polyedra2 to draw the constructions line
// depending on the use of tool_delaunay_constructions
// graph_mode : what mode to save

void save_geometric_graph(){
  ofstream file("graphs/lol.geo_graph");
  int n = points.size();
  // nombre de sommets
  file << n << endl;
 
  // position géométrique puis type molecule
  for ( int i = 0 ; i < n ; i ++ ){
    file << points[i].x << endl;
    file << points[i].y << endl;
  }
  
  // adjancence
  for ( int i = 0 ; i < n ; i ++ )
    for ( int j = 0 ; j < n ; j ++ )
      file << G.is_edge(i,j) << endl;  
}





void save_graph(){
  ofstream file("graphs/lol.graph");
  int n = points.size();
  // nombre de sommets
  file << n << endl;
 
  // position géométrique puis type molecule
  for ( int i = 0 ; i < n ; i ++ ){
    file << points[i].x << endl;
    file << points[i].y << endl;
  }
  
  // adjancence
  for ( int i = 0 ; i < n ; i ++ )
    for ( int j = 0 ; j < n ; j ++ )
      file << G.is_edge(i,j) << endl;  
}


void load_graph( string s ){
	cout << "loading file " << s << endl;
  ifstream file(s);
  
  int n;
  file >> n;
  points.resize(n);  
  G = Graph();
 
  for ( int i = 0 ; i < n ; i ++ ){
    G.add_vertex(i);
    file >> points[i].x;
    file >> points[i].y;
  }
  
  for ( int i = 0 ; i < n ; i ++ )
    for ( int j = 0 ; j < n ; j ++ ){
      bool b;
      file >> b;
      if ( b && i < j )      
        G.add_edge(i,j);
    }

   remplirA();
    FloydWarshall();
}
