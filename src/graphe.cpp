
using namespace std;

// ===-===-===-===-===-===-=== //
// ===-===-===-===-===-===-=== //
class Vertex{
  int _num;
  string _name;
  
  public:
    Vertex();
    Vertex(int n);
    Vertex(int n, string s);
    int num();
    
    bool operator<( const Vertex& other) const
      { return _num < other._num; }
      
    bool operator==(const Vertex &b){ return _num == b._num; }
};



Vertex::Vertex()
  {
    _num = -1;
    _name = "";
  }
  
Vertex::Vertex(int n)
  {
    _num = n;
    _name = "";
  }
  
Vertex::Vertex(int n, string s)
  {
    _num = n;
    _name = s;
  }  


int Vertex::num(void) { return _num; }


// ===-===-===-===-===-===-=== //
// ===-===-===-===-===-===-=== //
class Edge{
    Vertex _start;
    Vertex _end;   
    int _dir; // 0 si non dirigé, 1 de start vers end, -1 sinon
    sf::Color _color;
    
  public:
    Edge();
    Edge(Vertex s, Vertex e);
    Vertex start();
    Vertex end(void);
    int dir();
    sf::Color color();
    void change_dir(int s, int e);
    void change_dir(int d);
    void change_color( sf::Color c);
    
    bool operator<( const Edge& other) const
      { Vertex vos = other._start;
        Vertex vs = _start;
        Vertex voe = other._end;
        Vertex ve = _end;
        if ( vs.num() < vos.num() )
          return true;
        else if ( vs.num() > vos.num() )
          return false;
        else 
          return ve.num() < voe.num(); }
      
    bool operator==(const Edge &b){ return (_start == b._start) && (_end == b._end) ; }
};

Edge::Edge() {
}

Edge::Edge(Vertex s, Vertex e) {
    _start = s;
    _end = e;
    _dir = 0;
    _color = sf::Color::Black;
  }
  
 
Vertex Edge::start(void) { return _start; }

Vertex Edge::end(void) { return _end; }  

int Edge::dir() { return _dir; }

sf::Color Edge::color() { return _color; }

void Edge::change_dir(int s, int e)
  {
    if ( _start == s )
      _dir = 1;
    else
      _dir = -1;  
  }

void Edge::change_dir(int d)
  {
    _dir = d;
  }

  
void Edge::change_color( sf::Color c )
  {
    _color = c;
  }
  
  
// ===-===-===-===-===-===-=== //
// ===-===-===-===-===-===-=== //
class Graph{
  list<Vertex> V;
  list<Edge> E;
  
  public:
    Graph();
    Graph(int n);
    Graph(vector<int> t);
    void add_arc(int s, int e, sf::Color c);
    void  add_edge(int s, int e);
    void  add_edge(Vertex s, Vertex e);    
    void  add_random_edges(int k);
    void  add_vertex(int x);
    void  add_vertex(int x, string s);
    void  add_vertex(string s);
    list<Edge>  edges(void);
    bool is_edge(int s , int e );
    int   number_connected_components(void);
    void  parcours(Vertex x, int nc, map<Vertex,int> *compo);   
    void nbcc_parcours( int i , int nc , vector<int> *compo, vector< vector<bool> > adj,int n);
    int nbcc(vector< vector<bool> > adj);        
    void  sage_format();
    void  sage_format(string s);
    void  remove_edge(int s, int e);
    void  remove_vertex(int u);
    list<Vertex> vertices(void);
    
    // chromatic number
    bool canColor(int ncolors);
    bool canColorRemaining(int ncolors, int colored_so_far, vector<int> color); 
    
};


Graph::Graph()
  {
    V = list<Vertex> ();
    E = list<Edge> ();
  }


Graph::Graph(int n) {
    for ( int i = 1 ; i <= n ; i ++ )
      V.push_front(Vertex(i));
    E = list<Edge> ();
  }

Graph::Graph(vector<int> t) {
    for ( int i = 0 ; i < t.size() ; i ++)
      V.push_front(Vertex(t[i]));
    E = list<Edge> ();
  }  


void Graph::add_arc(int s, int e, sf::Color c) {
    Vertex vs;
    Vertex ve;
    for( list<Vertex>::iterator it = V.begin(); it != V.end(); it++ )
      {
        Vertex v = *it;
        if ( v.num() == s )
          vs = v;
        if ( v.num() == e )
          ve = v;
      }
    Edge ed (vs,ve);
    ed.change_dir(s,e);
    ed.change_color(c);
    E.push_front( ed );
  }
  

void Graph::add_edge(int s, int e) {
    Vertex vs;
    Vertex ve;
    for( list<Vertex>::iterator it = V.begin(); it != V.end(); it++ )
      {
        Vertex v = *it;
        if ( v.num() == s )
          vs = v;
        if ( v.num() == e )
          ve = v;
      }
    E.push_front( Edge(vs,ve));
  }
  
  
void Graph::remove_edge(int s, int e)
  {
    Vertex vs;
    Vertex ve;
    for( list<Vertex>::iterator it = V.begin(); it != V.end(); it++ )
      {
        Vertex v = *it;
        if ( v.num() == s )
          vs = v;
        if ( v.num() == e )
          ve = v;
      }
    E.remove( Edge(vs,ve) );
    E.remove( Edge(vs,ve) );      
  }
  
void Graph::remove_vertex( int u )
  {
    list<Edge> lol;
   for (list<Edge>::iterator it = E.begin(); it != E.end(); it++)
      {
        int x = (*it).start().num();
        int y = (*it).end().num();
        if ( x != u && y != u )
          {
          int xx = x > u ? x-1 : x;
          int yy = y > u ? y-1 : y;
          Vertex vs(xx);
          Vertex ve(yy);
          lol.push_back( Edge(vs,ve));          
          }
      }       
      E = lol;
    
    list<Vertex> lolV;

    for( list<Vertex>::iterator it = V.begin(); it != V.end(); it++ )
      {
        Vertex v = *it;
        int x = v.num();
        if ( x != u )
          {
            int xx = x > u ? x-1 : x;
            lolV.push_back( Vertex(xx) );
          }
      }
    V = lolV;
    
     
  }
  
void Graph::add_edge(Vertex s, Vertex e) {
    E.push_front( Edge(s,e));
  }

void Graph::add_vertex(int x) {
    V.push_front(Vertex(x));
  }

void Graph::add_vertex(int x, string s) {
    V.push_front(Vertex(x,s));
  }


list<Vertex> Graph::vertices(void)
  {
    
      return V;
    /*for (list<Vertex>::iterator it = V.begin(); it != V.end(); it++)
      cout << (*it).num() << ' ';
    cout << endl;
    */
  }
  

list<Edge> Graph::edges(void)
  {
    return E;
/*    for (list<Edge>::iterator it = E.begin(); it != E.end(); it++)
      cout << "(" << (*it).start().num() << "," << (*it).end().num() << ")" << ' ';
    cout << endl;
    */
  }  






bool Graph::is_edge(int s , int e )
  {
     for (list<Edge>::iterator it = E.begin(); it != E.end(); it++)
      {
        Edge ed = *it;
        if ( ed.start() == s && ed.end() == e )
          return true;
        if ( ed.start() == e && ed.end() == s )
          return true;
        
              
      }  
      return false;
  }


void Graph::parcours(Vertex x, int nc, map<Vertex,int> *compo)
      {
      
        if( (*compo)[x] == -1 )
          {
            (*compo)[x] = nc;
            for (list<Edge>::iterator it = E.begin(); it != E.end(); it++)
              {
                Edge e= (*it);
                if( e.start() == x && (*compo)[e.end()] == -1 )
                  parcours( e.end(), nc, compo);                  
                else if ( e.end() == x && (*compo)[e.start()] == -1 )
                  parcours( e.start(), nc, compo);
              }
          }
      }
  
  
  
int Graph::number_connected_components(void)
  {
    map<Vertex, int> compo;
    for (list<Vertex>::iterator v = V.begin(); v != V.end(); v++)
      compo[*v] = -1;
    
    int nc = 0;
    for (list<Vertex>::iterator v = V.begin(); v != V.end(); v++)
      if ( compo[*v] == -1 )
        {
          nc ++;
          parcours( *v, nc, &compo );
        }
    return nc;
  }
  
 
// 2eme version avec matrice 
// pour les graphes avec sommets numérotés de 0 à n-1
void Graph::nbcc_parcours( int i , int nc , vector<int> *compo, vector< vector<bool> > adj,int n){
  if( (*compo)[i] == -1 ){
    (*compo)[i] = nc;
    for ( int j = 0 ; j < n ; ++ j )
      if ( adj[i][j] && (*compo)[j] == -1 ){
          nbcc_parcours( j , nc , compo, adj,n);
      }
  }   
}

int Graph::nbcc(vector< vector<bool> > adj){
  int n = V.size();
  
  /*
  vector< vector<bool> > adj;
  adj.resize(n);
  for (int i = 0; i < n ; ++ i ){
    adj[i].resize(n);
    for ( int j = 0 ; j < i ; ++ j )
      if ( is_edge(i,j) ){
        adj[i][j] = true;
        adj[j][i] = true;
      }
  }
  */
  
  cout << "coucou" << endl;
  
  vector<int> compo;
  compo.resize(n);
  for ( int i = 0 ; i < n ; ++ i )
    compo[i] = -1;
  int nc = 0;
  
  for ( int i = 0 ; i < n ; ++ i )
    if ( compo[i] == -1 ){
      nc ++;
      nbcc_parcours( i , nc, &compo,adj,n);
    }
  return nc;
}
  


void Graph::sage_format()
  {
    cout << "G.add_vertices([";
    for (list<Vertex>::iterator v = V.begin(); v != V.end(); v++)
      {
        cout << (*v).num() << "," ;
      }
      cout << "])" << endl;
      
      
    for (list<Edge>::iterator it = E.begin(); it != E.end(); it++)
      {
        cout << "G.add_edge(" << (*it).start().num() << "," <<  (*it).end().num() << ")" << endl;
      } 
                   
  
  }
  
// ======================
// save the graph in Sage format (you will have to copy paste in Sage) in a file s
// no test about file s !
void Graph::sage_format(string s){
  ofstream file(s);
  file << "G.add_vertices([";
  for (Vertex v : V){
    file << v.num() << "," ;
  }
  file << "])" << endl;      
  for (Edge e : E)  {
    file << "G.add_edge(" << e.start().num() << "," <<  e.end().num() << ")" << endl;
  } 
}




void Graph::add_random_edges(int k)
  {
    int n = V.size();
    for ( int i = 0 ; i < k ; i ++ )
      {
        int a = rand()%n;
        int b = rand()%n;
        list<Vertex>::iterator it1 = V.begin();
        list<Vertex>::iterator it2 = V.begin();        
        advance(it1, a);
        advance(it2, b);        
        add_edge(*it1,*it2);
      }
  }
  
  
  // D. Matula, G. Marble, and J. Isaacson
// Graph coloring algorithms in Graph Theory and Computing.
// Academic Press, pp.104-122, 1972. 
  
  
  /*
  
  int Graph::chromaticNumber() {
  for (int ncolors = 1; true; ncolors++) 
  {
    if (canColor(g, ncolors)) return ncolors;
  }
}

*/

bool Graph::canColor(int ncolors) 
{
  cout << "canColor(" << ncolors << ")" << endl;
  int n = vertices().size();
  vector<int> color (n);
  bool b = canColorRemaining(ncolors, n-1,color);
  if ( b )
    {
    cout << "coloring trouvé" << endl;
    for ( int i = 0 ; i < n ; i ++ )
      cout << i << " -> " << color[i] << endl;
    }
    
  return b;
}


 
 
// sommets de 0 à n-1
// tous les sommets au dessus de colored_so_far ont été coloriés
bool Graph::canColorRemaining(int ncolors, int colored_so_far, vector<int> color ) 
{
  int n = vertices().size();
  if (colored_so_far == -1)
    return true;
  
  //cout << "disjonction sur " << colored_so_far << " ----" << endl;
  //cout << "coloring actuel" << endl;
   // for ( int i = colored_so_far+1 ; i < n ; i ++ )
   //   cout << i << " -> " << color[i] << endl;
    
     //  cout << "boucle test couleur de 0 à " << ncolors-1 << " sur " << colored_so_far << endl;   
  for (int c = 0; c < ncolors; c++) 
  {
    bool ok = true;
 
    for (int v = colored_so_far+1 ; v < n ; v ++ )
      {    
     // cout << v << endl;
    if( is_edge(v,colored_so_far) && color[v] == c )
    {
     // cout << "edge" << endl;
     // cout << v << endl;
        ok = false; 
        break;
    }    
    }
    
    if (ok) 
    {
      
   //   cout << "nouvel coloration" << colored_so_far << " -> " << c << endl;
      color[colored_so_far] = c;
      if (canColorRemaining(ncolors, colored_so_far - 1, color) ) 
        return true;
    }
  }
  return false;
}
  
  
  
  
 
  
  
  
  
// ===-===-===-===-===-===-=== //
// ===-===-===-===-===-===-=== //
// Simplicial complex




// ===-===-===-===-===-===-=== //
// ===-===-===-===-===-===-=== //
// random tests


float ncc_moyen(int n, int m,  int nb_test )
  {
    float sum = 0;
    
    for ( int i = 0 ; i < nb_test ; i ++ )
      {
        Graph G(n);
        G.add_random_edges(m);
        sum += G.number_connected_components();        
      }
    return sum/nb_test;
  }
  
float ncc_moyen2(int n, int m,  int nb_test )
  {
    float sum = 0;
    
    for ( int i = 0 ; i < nb_test ; i ++ )
      {
        Graph G(n);
        G.add_random_edges(m);
        sum += (G.number_connected_components() == 1);        
      }
    return sum/nb_test;
  }  



